package dk.s4.phg.baseplate.tutorial.count_and_calculate;

import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.messages.interfaces.tutorial_count_and_calculate
    .CalcPrime;
import dk.s4.phg.messages.interfaces.tutorial_count_and_calculate
    .CalcPrimeResult;

/**
 * CountAndCalculate tutorial/demo Calculator module.
 *
 * The Calculator module is one of the two business modules of the
 * CountAndCalculate tutorial/demo.
 *
 * This module subscribes to events containing a number, and will
 * calculate if that number is a prime and publish that information.
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public final class Calculator extends ModuleBase {

    /**
     * The interface endpoint
     */
    private InterfaceEndpoint endpoint;

    /**
     * The module constructor.
     *
     * Will create and start the Calculator module.
     * 
     * @param c The PHG Core common Context object shared by all
     *                modules connected to the JVM baseplate
     */
    public Calculator(Context c) {
        super(c, "PhgJvmBaseplateTutorialCalculator");

        // Define our interface to the world: producer of the
        // TutorialCountAndCalculate interface
        endpoint = implementsProducerInterface("TutorialCountAndCalculate");

        // Declare the handler for CalcPrime
        endpoint.addEventHandler("CalcPrime",
                                 new EventImplementation<CalcPrime>() {
                                     public void accept(CalcPrime request,
                                                        MetaData meta) {
                                         // The event is handled by the
                                         // gotRequest() method
                                         gotRequest(request
                                                    .getCandidateValue());
                                     }
                                 });

        // All interfaces have been declared. Start the module
        start();
    }

    /**
     * Handle a single request to determine if an integer is prime.
     *
     * @param n The candidate integer
     */
    private void gotRequest(int n) {
        // Log the calculation at debug level
        getLogger().debug("Testing " + n);

        // Calculate the result
        boolean result = isPrime(n);

        // Publish the result
        endpoint.postEvent("CalcPrimeResult",
                           CalcPrimeResult.newBuilder()
                           .setIsPrime(result)
                           .build());
    }
    
    /**
     * Do the calculation: Is this integer a prime.
     *
     * This is just the naïve and inefficient algorithm to check a
     * prime - nothing to see here...
     * 
     * @param n The candidate integer
     * @return The result of the calculation
     */
    private boolean isPrime(int n) {
        boolean isPrime = true;
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }
}
