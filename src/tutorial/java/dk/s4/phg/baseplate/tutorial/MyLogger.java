package dk.s4.phg.baseplate.tutorial;

import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.LogCollectorModule;
import dk.s4.phg.baseplate.LogCollectorModule.Level;

/**
 * Simple log collector printing to standard out.
 *
 * This simple tutorial log collector module will pretty-print log
 * messages to the standard output stream using ANSI colour and effect
 * codes.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public final class MyLogger extends LogCollectorModule {

    /**
     * Create a simple log collector module, printing log messages to
     * the standard output stream.
     *
     * @param context   The PHG Core common Context object shared by all
     *                  modules connected to thes JVM baseplate
     */
    public MyLogger(Context context) {

        // Create the base class
        super(context, "PhgJvmBaseplateTutorialMyLogger");
        // We want all logging output
        setLogCollectingLevel(Level.DEBUG);
        // Start the module
        start();
    }

    /**
     * Pretty-print the log message to an ANSI terminal on System.out
     * @param msg The log message to print
     */    
    protected void log(LogMessage msg) {

        // We build the output one element at the time in a
        // StringBuffer
        StringBuilder sb = new StringBuilder();

        // Keeping track on message effects used for FATAL and ERROR
        // levels
        String messageEffect = "";

        // Debug level
        switch (msg.level) {    
        case DEBUG:
          sb.append(Effect.BOLD);
          sb.append("DEBUG");
          break;
        case INFO:
          sb.append(Effect.BLUE_BRIGHT);
          sb.append(Effect.BOLD);
          sb.append("INFO");
          break;
        case WARN:
          sb.append(Effect.YELLOW);
          sb.append(Effect.BOLD);
          sb.append("WARNING");
          break;
        case ERROR:
          sb.append(Effect.RED);
          sb.append(Effect.BOLD);
          sb.append("ERROR");
          messageEffect = Effect.BOLD.toString();
          break;
        case FATAL:
          sb.append(Effect.RED_BRIGHT);
          sb.append(Effect.BOLD);
          sb.append("FATAL");
          messageEffect = Effect.RED.toString() + Effect.BOLD.toString();
          break;
        default:
          break;
        }
        sb.append(Effect.RESET);

        // Module TAG
        sb.append(messageEffect + " in " + msg.moduleTag);
        
        // File name and line number
        if (msg.fileName != null && !msg.fileName.isEmpty()) {
            sb.append(" (");
            sb.append(Effect.UNDERLINE);
            sb.append(msg.fileName + ":" + msg.lineNumber);
            sb.append(Effect.RESET);
            sb.append(messageEffect + ")");
        }

        // Class name
        if (msg.className != null && !msg.className.isEmpty()) {
            sb.append(" " + msg.className);
        }
        
        // Function name
        if (msg.functionName != null && !msg.functionName.isEmpty()) {
            if (msg.className != null && !msg.className.isEmpty()) {
                sb.append("::");
            } else {
                sb.append(" ");
            }
            sb.append(msg.functionName);
        }

        // Message
        sb.append("\n    " + msg.message);

        // CausedBy
        if (msg.causedBy != null) {
            sb.append("\n    Caused by:\n");
            sb.append(msg.causedBy);
        }
        
        sb.append(Effect.RESET);

        // The message is printed on System.out
        System.out.println(sb);
    }



    /**
     * ANSI terminal escape sequences used to add colour and effects.
     */
    public enum Effect {
        RESET("\033[0m"),
        
        BLACK("\033[0;30m"),
        RED("\033[0;31m"),
        GREEN("\033[0;32m"),
        YELLOW("\033[0;33m"),
        BLUE("\033[0;34m"),

        BOLD("\033[1m"),
        UNDERLINE("\033[4m"),

        BLACK_BRIGHT("\033[0;90m"),
        RED_BRIGHT("\033[0;91m"),
        GREEN_BRIGHT("\033[0;92m"),
        YELLOW_BRIGHT("\033[0;93m"),
        BLUE_BRIGHT("\033[0;94m");

        private final String ansiSequence;

        Effect(String ansiSequence) { this.ansiSequence = ansiSequence; }

        @Override
        public String toString() {
            return ansiSequence;
        }
    }
}
