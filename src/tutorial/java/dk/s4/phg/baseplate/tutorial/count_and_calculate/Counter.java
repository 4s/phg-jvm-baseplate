package dk.s4.phg.baseplate.tutorial.count_and_calculate;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.messages.interfaces.tutorial_count_and_calculate
    .CalcPrime;
import dk.s4.phg.messages.interfaces.tutorial_count_and_calculate
    .CalcPrimeResult;

/**
 * CountAndCalculate tutorial/demo Counter module.
 *
 * The Counter module is one of the two business modules of the
 * CountAndCalculate tutorial/demo.
 *
 * This module will collect 10 prime numbers by publishing numbers
 * from 2 and upward, subscribing to the calculation result, and write
 * this information to the log. When it has collected the 10 numbers,
 * it will request a system shutdown.
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public final class Counter extends ModuleBase {

    /**
     * The next candidate integer to test
     */
    private int nextCandidate;

    /**
     * The number of primes found so far
     */
    private int primeCount;

    /**
     * The interface endpoint
     */
    private InterfaceEndpoint endpoint;
    
    /**
     * The module constructor.
     *
     * Will create and start the Counter module.
     * 
     * @param c The PHG Core common Context object shared by all
     *                modules connected to the JVM baseplate
     */
    public Counter(Context c) {
        super(c, "PhgJvmBaseplateTutorialCounter");

        // Start looking for primes from 2
        nextCandidate = 2;
        
        // We have so far found 0 primes...
        primeCount = 0;

        // Define our interface to the world: consumer of the
        // TutorialCountAndCalculate interface
        endpoint = implementsConsumerInterface("TutorialCountAndCalculate");

        // Declare the handler for CalcPrimeResult
        endpoint.addEventHandler("CalcPrimeResult",
                                 new EventImplementation<CalcPrimeResult>() {
                                     public void accept(CalcPrimeResult result,
                                                        MetaData meta) {
                                         // The event is handled by the
                                         // gotResult() method
                                         gotResult(result.getIsPrime());
                                     }
                                 });
        
        // All interfaces have been declared. Start the module
        start();
    }

    @Override
    protected void onApplicationStateChange(ApplicationState newState) {

        // When the application starts
        if (newState == ApplicationState.RUNNING) {
            // Begin calculating primes
            testOrQuit();
        }
    }
    // FIXME: remove
    void kick() {testOrQuit();}

    
    /**
     * If we have found 10 primes, quit the application, otherwise
     * test the next candidate.
     */
    private void testOrQuit() {
        
        if (primeCount >= 10) {
            // Exit the application when 10 primes have been found

            getLogger().info("10 primes found. Exiting...");

            // FIXME
            System.out.println("DONE");
            
        } else {
            
            // Test the next candidate
            testNumber(nextCandidate);            
        }
    }

    /**
     * Test if a number is prime using a remote function.
     * 
     * @param n The candidate integer
     */
    private void testNumber(int n) {
        endpoint.postEvent("CalcPrime",
                           CalcPrime.newBuilder()
                           .setCandidateValue(n)
                           .build());
    }

    /**
     * Handle a response.
     *
     * @param isPrime The response: was this a prime?
     */
    private void gotResult(boolean isPrime) {
        if (isPrime) {
            // Increase the counter and print the result to the log
            primeCount++;
            getLogger().info("Found prime number " + primeCount + ": " +
                             nextCandidate);
            
        }
        
        // Usually not allowed on threads, but we sleep a short while
        // for illustrative purposes to slow down the execution
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {}

        // Proceed to the next value
        nextCandidate ++;
        testOrQuit();  
    }
}
