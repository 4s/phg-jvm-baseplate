package dk.s4.phg.baseplate.tutorial.count_and_calculate;

import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.master.ReflectorContext;
//import dk.s4.phg.baseplate.master.MasterModule;
import dk.s4.phg.baseplate.tutorial.MyLogger;

import java.util.ArrayList;

/**
 * CountAndCalculate tutorial/demo using only Java modules.
 *
 * This tutorial demonstrates a simple event exchange between two
 * business modules:
 *
 * One module subscribes to events containing a number, and will
 * calculate if that number is a prime and publish that information.
 *
 * Another module will collect 10 prime numbers by publishing numbers
 * from 2 and upward, subscribing to the calculation result, and write
 * this information to the log. When it has collected the 10 numbers,
 * it will request a system shutdown.
 *
 * A third module is a log collector, which will collect the logging
 * information and print it to standard out.
 *
 * Finally, the MasterModule - which must be present in all PHG Core
 * systems - is responsible for starting and shutting down the system.
 *
 * In this example, all these four modules are implemented in Java. It
 * is possible to mix and match modules from different programming
 * languages. This is the topic of another tutorials.
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class CountAndCalculate {
  
    public static void main(String[] arguments) throws Exception {

        // The baseplate must include exactly one "reflector" and one
        // instance of the MasterModule. In this example, both of
        // these are implemented in Java. The Reflector is a subclass
        // of the PHG Core common Context class.

        Context context = new ReflectorContext(0) {
                // We may override the corePanic to direct unexpected
                // exceptions to a log or terminal output. This is a
                // way to ensure that information about unexpected
                // exceptions are not lost. Under normal circumstances
                // all errors should be caught by proper handlers and
                // not end up here. If corePanic is ever called, it
                // probably means that a module, or the entire PHG
                // Core system, has crashed!
                protected synchronized void corePanic(String moduleTag,
                                                      Exception cause) {
                    System.err.print("*** PHG Core PANIC in ");
                    System.err.println(moduleTag);
                    cause.printStackTrace();
                }
            };

        // We collect all our modules in a single list
        ArrayList<ModuleBase> modules = new ArrayList<>();

        // FIXME: remove when startup is implemented
        Counter j;
        
        modules.add(j = new Counter(context));
        modules.add(new Calculator(context));
        modules.add(new MyLogger(context));
        //modules.add(new MasterModule(context, 4));

        /*
    
          for (ModuleBase module : modules) {
          module.join();
          }
          
        */

        // FIXME: remove when startup and shutdown is implemented
        try {Thread.sleep(100);} catch (InterruptedException e) {}
        j.kick();
        try {Thread.sleep(5000);} catch (InterruptedException e) {}
        
        
        context.close();
    }
}
