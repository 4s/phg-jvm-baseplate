syntax = "proto3";

package s4.messages.interfaces.tutorial_count_and_calculate;
option java_package = "dk.s4.phg.messages.interfaces.tutorial_count_and_calculate";
option java_multiple_files = true;
//option optimize_for = LITE_RUNTIME;


/**
 * In this simple tutorial, two events are defined, one in each
 * direction between producer and consumer.
 *
 * The Consumer needs help to calculate whether a candidate integer is
 * a prime number. Hence, it will multicast an event asking for help
 * determining if a number is prime.
 *
 * The Producer offers the capability to produce results of this prime
 * number calculation. It will multicast a boolean indicating if the
 * requested number was a prime or not.
 */


//
// The C2P and P2C messages defined below must be exactly according to
// a fixed format. You can read more about how to correctly construct
// these in the phg-messaging library documentation. For the purpose
// of this tutorial, just think of it as opaque boilerplate code...
//

message C2P {
  message Event {
    oneof event {
      CalcPrime calc_prime = 1;
    }
  }
  oneof payload {
    // No requests defined at this point
    Event event = 2;
  }
}

message P2C {
  message Event {
    oneof event {
      CalcPrimeResult calc_prime_result = 1;
    }
  }
  oneof payload {
    // No requests defined at this point
    Event event = 2;
  }
}



/**
 * This defines the message multicasted from the Consumer (Counter) to
 * the Producer (Calculator) module. It contains the integer, we wish
 * to test.
 */
message CalcPrime { // C2P
  
  /**
   * The number to test
   * Mandatory
   */
  int32 candidate_value = 1;
}


/**
 * This defines the message multicasted from the Producer (Calculator)
 * to the Consumer (Counter) module. It contains the result of the
 * prime test as a boolean - true: the number was a prime; false: it
 * was not prime.
 */
message CalcPrimeResult { // P2C
  
  /**
   * The result of the test: Is this a prime?
   * Mandatory
   */
  bool is_prime = 1;
}
