//package dk.s4.phg.baseplate.master;
//
//import dk.s4.phg.baseplate.Context;
//import dk.s4.phg.baseplate.ModuleBase;
//
//import java.util.HashMap;
//import java.util.Map;
//
//public class MasterModule extends ModuleBase {
//
//  private Map<String, Boolean> running = new HashMap<>();
//
//  *
//   * Create a new master module for the given context expecting a certain number of modules
//   * (including the master it self).
//   *
//   * @param c
//   * @param expected
//
//  public MasterModule(Context c, int expected) {
//    super(c.getMasterId(), c);
//
//
//     * The Ping function listens after Ping messages and respond to the sender if.
//
//    addFunction("StartupMaster", (MetaData metadata) -> {
//      if (metadata.getSignal().equals("PING")) {
//
//        running.put(metadata.sender, false);
//        debug("Got PING from " + metadata.sender + " " + running.size() + "/" + expected);
//        sendUnicast(metadata.sender, "Startup", "PONG");
//
//      } else if (metadata.getSignal().equals("READY")) {
//
//        if (!running.containsKey(metadata.getSender())) {
//          err("Unexpected READY signal from unknown module '" + metadata.getSender()
//              + "' will be ignored.");
//          return;
//        }
//
//        running.put(metadata.getSender(), true);
//        debug(metadata.getSender() + " is ready");
//
//        boolean allReady = all(true);
//
//        if (running.size() == expected && allReady) {
//          sendBroadcast("Startup", "START");
//          info("Got READY signal from all " + running.size()
//                            + " modules. Starting the application.");
//        }
//      }
//    });
//
//
//     * The ShutdownMaster function listens for shutdown REQUESTs. When such a request is
//     * received, it is broadcast to all modules. These now have to send either READY or CANCEL. If
//     * all responds with READY, a FINALIZE signal is broadcast to all modules which then have to
//     * shut down. If one module answers with a CANCEL signal, this is broadcast to all modules and
//     * the shutdown process is cancelled.
//
//    addFunction("ShutdownMaster", (MetaData metadata) -> {
//
//      if (metadata.getSignal().equals("REQUEST")) {
//
//        debug("Got shutdown request from " + metadata.getSender());
//        sendBroadcast("Shutdown", "REQUEST");
//
//      } else if (metadata.getSignal().equals("READY")) {
//
//        if (running.containsKey(metadata.getSender())) {
//          running.put(metadata.getSender(), false);
//          debug(metadata.getSender() + " is ready to shutdown");
//
//          boolean allShutdown = all(false);
//
//          if (allShutdown) {
//            info("All modules are ready to shutdown.");
//            sendBroadcast("Shutdown", "FINALIZE");
//          }
//        }
//
//      } else if (metadata.getSignal().equals("CANCEL")) {
//
//        info(metadata.getSender() + " cancelled shutdown");
//        for (String module : running.keySet()) {
//          running.put(module, true);
//        }
//        sendBroadcast("Shutdown", "CANCEL");
//      }
//    });
//
//    startModule();
//  }
//
//  *
//   * Return true iff all the answer to "Is module X running?" has the same answer for all modules,
//   * and if the answer is the same as the given value.
//   *
//   * @param value
//   * @return
//
//  private boolean all(boolean value) {
//    boolean all = true;
//    for (boolean v : running.values()) {
//      if (v != value) {
//        all = false;
//        break;
//      }
//    }
//    return all;
//  }
//}
