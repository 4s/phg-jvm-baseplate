package dk.s4.phg.baseplate.master;

import dk.s4.phg.baseplate.Context;

import org.zeromq.ZMQ;
import org.zeromq.SocketType;

/**
 * The PHG Core common Context object for master baseplates.
 * 
 * The master baseplate implementation of a Context implements a
 * simple reflector that will reflect all input back on the output.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class ReflectorContext extends Context {

    private ZMQ.Socket in, out;

    /**
     * Create the Context for a master baseplate.
     * @param baseplateUniqueIndex Must be a small number (no more
     *                             than 8 bits) unique among all
     *                             baseplate instances connected to
     *                             the entire system
     * @throws IllegalArgumentException if the argument is &gt;255
     * @throws NullPointerException if the Context could not be created
     */
    public ReflectorContext(int baseplateUniqueIndex) {
        super(baseplateUniqueIndex);
    }

    protected void initialize() {
        // Create and bind the PUB socket
        out = getZMQContext().createSocket(SocketType.PUB);
        out.bind(getReflector2module());
    
        // Create, subscribe all, and bind the SUB socket
        in = getZMQContext().createSocket(SocketType.SUB);
        in.subscribe(new byte[0]); // subscribe all messages
        in.bind(getModule2reflector());


        // Reflector implementation: As simple as it gets:
        this.registerPollSocket(in, (data, status) -> {
                out.send(data, status ? ZMQ.SNDMORE : 0);
            });
    }
    
    protected void cleanup() {

        // The ZeroMQ manual recommends setting a LINGER time of about
        // 1 second to resolve any locks during shutdown
        out.setLinger(1000);
        in.setLinger(1000);
        
        // Close the sockets
        out.close();
        in.close();
    }
}
