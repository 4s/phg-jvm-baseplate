
package dk.s4.phg.baseplate;

import org.zeromq.ZContext;
import org.zeromq.ZThread;
import org.zeromq.ZMQ;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.io.Closeable;

/**
 * The PHG Core common Context object.
 * 
 * The Context defines the common socket for connecting modules to
 * the PHG Core system. The Context class itself is abstract and the
 * concrete implementation is found in either the slave
 * (BridgeContext) or master (ReflectorContext) baseplates.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public abstract class Context implements Closeable {

    /**
     * By convention, the TAG should be the PascalCase equivalent of
     * the module (or in this case, baseplate) repository name.
     */
    static final String TAG = "PhgJvmBaseplate";
    
    /**
     * The ZeroMQ context. This is the "main" ZContext object. Each
     * module will make a so-called "shadow" copy of this object. See
     * https://github.com/zeromq/jeromq/wiki/Sharing-ZContext-between-thread
     * for more details on sharing ZeroMQ contexts in the JeroMQ
     * implementation.
     */
    private final ZContext zcontext;

    /**
     * The inproc socket URL of the baseplate receiving/SUB socket 
     */
    private final String module2reflector;

    /**
     * The inproc socket URL of the baseplate sending/PUB socket 
     */
    private final String reflector2module;

    /**
     * A socket used to send notifications from an outside (e.g. the
     * main) thread to the task loop thread.
     */
    private final ZMQ.Socket killPipe;

    /**
     * Running state of the task loop
     * 
     * Updating this value must be guarded by locking the context
     * object.
     */
    private volatile boolean running = true;

    /**
     * Pair of a socket with its handler. Used as elements on the
     * inSockets list of socket handlers.
     */
    private class InSocketListItem {
        ZMQ.Socket socket;
        BiConsumer<byte[], Boolean> callback;
        InSocketListItem(ZMQ.Socket socket, BiConsumer<byte[],
                         Boolean> callback) {
            this.socket = socket;
            this.callback = callback;
        }
    }
    
    /**
     * Pairs of sockets being polled by ZeroMQ and their respective
     * handlers.
     *
     * The contents of this list must be filled by the (overridden)
     * initialize() method using the registerPollSocket() method.
     */
    private ArrayList<InSocketListItem> inSockets
      = new ArrayList<InSocketListItem>();

    /**
     * The next value to be handed out as unique id for this
     * baseplate.
     */
    private AtomicInteger uidCounter;
    
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*         The only public API is the close() method            */
    /*                                                              */
    /* ************************************************************ */

    /**
     * Stop the Context and kill the task loop.
     *
     * This method will block to synchronize with the end of the task
     * loop thread.
     *
     * Note: This method should be called only once and if invoked
     * more than once, subsequent calls will return immediately
     * (i.e. not synchronize with the end of the task loop thread)
     */
    @Override
    public void close() {
        synchronized (zcontext) {
            if (!running) return; // Already stopped
            running = false;
        }
        
        // Notify the task loop thread (By ignoring the return value,
        // we also ignore any error)
        killPipe.send(new byte[] {'C'});
        
        // Waiting for all sockets to close. This may block for while...
        zcontext.destroy();
    }
    
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*       The following methods are used by the subclasses       */
    /*                                                              */
    /* ************************************************************ */
    
    /**
     * Constructor
     * @param baseplateUniqueIndex Must be a small number (no more
     *                             than 8 bits) unique among all
     *                             baseplate instances connected to
     *                             the entire system
     * @throws IllegalArgumentException if the argument is &gt;255
     * @throws NullPointerException if the Context could not be created
     */
    protected Context(int baseplateUniqueIndex)
               throws IllegalArgumentException, NullPointerException {

        if (baseplateUniqueIndex > 255) {
            // BaseplateUniqueIndex must be as low as possible and no
            // more than 8 bits
            throw new IllegalArgumentException("Illegal baseplateUniqueIndex");
        }

        // Initialize the UID counter. We use the convolute operation
        // to create a large block of unique values owned by this
        // baseplate which can be handed out to modules
        uidCounter = new AtomicInteger(convolute(baseplateUniqueIndex));
        
        // The Inproc URLs. Fixed strings - they just have to be
        // unique in the scope of on THIS Context instance
        module2reflector = "inproc://phg-ctx-m2r";
        reflector2module = "inproc://phg-ctx-r2m";
        
        // Create the main ZContext. The ZContext Java object is NOT
        // thread-safe, but is wrapping the thread-safe ZeroMQ
        // context. Each module should create its own ZContext wrapper
        // (a so-called shadow ZContext). This is done by fork'ing the
        // main ZContext.
        zcontext = new ZContext();

        // Start the task loop...
        killPipe = zcontext.fork(taskLoop);

        // The task loop could not be started for some reason
        if (killPipe == null) {
            throw new NullPointerException("Context could not be created");
        }
    }
    
    /**
     * Send a "kick" to the task loop.
     *
     * This may be used by subclasses to send a notification from the
     * main thread (or other external threads) to the task loop
     * thread. This will result in the method kicked() being called on
     * the task loop thread.
     *
     * Subclasses that use this functionality should expose it in some
     * other way using a public method.
     */
    protected void kick() {
        synchronized (zcontext) {
            if (!running) return; // Already stopped
            // Notify the task loop thread (By ignoring the return value,
            // we also ignore any error)
            killPipe.send(new byte[] {'K'});
        }
    }

    /**
     * Task loop is starting.
     *
     * Subclasses must override this method to inject work at the
     * beginning of the task loop (on the task Thread). For instance,
     * the registerPollSocket() method must be invoked from the body
     * of initialize()
     */
    protected abstract void initialize();

    /**
     * Task loop is finishing.
     *
     * Subclasses may override this method to inject work when the
     * task loop shuts down. This method will be executed at the very
     * end just before the Thread terminates.
     */
    protected void cleanup() {}

    /**
     * Task loop was kicked.
     *
     * The kick() method was invoked from an external thread causing
     * this method to be invoked on the task loop.
     */
    protected void kicked() {}
    
    /**
     * Register additional ZeroMQ sockets for the task loop poller.
     *
     * This method can be used by subclasses to register additional
     * sockets for the task loop poller. This can only be done from
     * inside an overridden initialize() method, as it MUST be
     * performed on the correct Thread AND before the task loop
     * actually begins.
     * @param socket The socket to poll
     * @param callback The callback receives a single message fragment
     *                 as a byte array, along with a boolean
     *                 indicating if more fragments belong to the
     *                 message.
     */
    protected void registerPollSocket(ZMQ.Socket socket,
                                      BiConsumer<byte[], Boolean> callback) {
        inSockets.add(new InSocketListItem(socket, callback));
    }
    
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*   The following methods are used by the ModuleBase class     */
    /*   as well as the subclasses                                  */
    /*                                                              */
    /* ************************************************************ */
    
    /**
     * Get the URL of the inproc socket of the baseplate receiving/SUB
     * socket
     * 
     * This method has package-private visibility, as it is used by
     * the ModuleBase class.
     *
     * @return The socket URL
     */
    protected String getModule2reflector() {
        return module2reflector;
    }
    
    /**
     * Get the URL of the inproc socket of the baseplate sending/PUB
     * socket
     * 
     * This method has package-private visibility, as it is used by
     * the ModuleBase class.
     *
     * @return The socket URL
     */
    protected String getReflector2module() {
        return reflector2module;
    }
    
    /**
     * Get the ZeroMQ main context.
     * 
     * This method has package-private visibility, as it is used by
     * the ModuleBase class.
     *
     * @return The ZeroMQ context main wrapper
     */
    protected ZContext getZMQContext() {
        return zcontext;
    }

    /**
     * An unhandled Exception caused the baseplate or a module to fail
     * and likely shut down.
     *
     * This method will be called if the Context thread or any thread
     * of the modules is struck by an unhandled Exception - often
     * causing it to terminate.
     *
     * It may be overridden by a derived class that is capable of
     * reporting it to an appropriate error logging facility.
     *
     * Use of the corePanic() is ALWAYS evidence of programming error,
     * so if the application support any kind of telemetry, it should
     * probably send a bug report if corePanic() is ever invoked in a
     * production system.
     *
     * @param moduleTag The module TAG of the module (or context)
     *                  shutting down unexpectedly.
     * @param cause     The Exception causing the termination.
     * @param fatal     true if the module or baseplate terminated,
     *                  and false if it will attempt a recovery. If
     *                  recovery is attempted, another corePanic may
     *                  follow if it did not succeed.
     */
    protected synchronized void corePanic(String moduleTag,
                                          Exception cause,
                                          boolean fatal) {}
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*   The following methods are used by the ModuleBase class     */
    /*   only                                                       */
    /*                                                              */
    /* ************************************************************ */

    /**
     * Generate a system-wide unique id.
     * 
     * This is done using a simple 31-bit unsigned counter. This
     * method is NOT intended to be called frequently. Only 8388608
     * unique values are guaranteed to be available for the entire
     * lifetime of the application (although many more will typically
     * be available) before collisions may occur.
     */
    int generateUID() {
        return uidCounter.getAndIncrement();
    }
    
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*                        The task loop                         */
    /*                                                              */
    /* ************************************************************ */
    
    private ZThread.IAttachedRunnable taskLoop
        = new ZThread.IAttachedRunnable() {
    
            @Override
            public void run(Object[] args, ZContext zcontext,
                            ZMQ.Socket killPipe) {

                try {
                    
                    // Subclasses may inject code at the beginning
                    initialize();
    
                    // Create the ZeroMQ socket poller with all the sockets
                    ZMQ.Poller poller = zcontext.createPoller(inSockets.size()
                                                              + 1);
                    for (InSocketListItem socketListItem : inSockets) {
                        poller.register(socketListItem.socket,
                                        ZMQ.Poller.POLLIN);
                    }
                    // Adding the killPipe at the end
                    poller.register(killPipe, ZMQ.Poller.POLLIN);

                    // Task loop
                    while (running) {
                        if (poller.poll() < 1) {
                            if (running && killPipe.errno() != 0) {
                                // If some error happened, and it is
                                // not because we are closing, die
                                throw new org.zeromq.ZMQException
                                    (killPipe.errno());
                            }
                            continue;
                        }

                        // Iterate through the registered sockets
                        int i;
                        for (i = 0; i < poller.getSize()-1; i++) {
                            if (poller.pollin(i)) {

                                // We did receive a message on this socket
                            
                                ZMQ.Socket in = inSockets.get(i).socket;
                                BiConsumer<byte[], Boolean> callback
                                    = inSockets.get(i).callback;
                            
                                // Loop through all message fragments
                                do {
                                    byte[] msg = in.recv();
                                    callback.accept(msg, in.hasReceiveMore());
                                } while (in.hasReceiveMore());
                            }
                        }
                    
                        // Also check the killPipe for a "kick" message
                        if (poller.pollin(i)) {
                            byte[] msg = killPipe.recv();
                            if (msg[0] == 'K') { // Kick
                                kicked();
                            }
                        }
                    }
                    
                    poller.close();

                    // Subclasses may inject code at the end
                    cleanup();
                } catch (RuntimeException e) {
                    // Unexpected termination due to unhandled exception
                    // Report it
                    corePanic(TAG, e, true);
                } finally {
                
                    // Free all resources
                    zcontext.destroy();
                }
            }
        };


    

    /* ************************************************************ */
    /*                                                              */
    /*                   Private helper functions                   */
    /*                                                              */
    /* ************************************************************ */
    
    /**
     * Convolutes the integer - i.e. mirror the order of digits.
     *
     * The order of the 31 least significant bits of the input is
     * swapped in the input (the 31st bit of the input will be
     * ignored, and the 31st bit of the output will always be 0).
     * 
     * For instance 0x31 becomes 0x46000000 
     */
    private int convolute(int input) {
        int output = 0;
        for (int count = 0; count < 30; count++) {
            output |= input & 0x40000000;
            output >>= 1;
            input <<= 1;
        }
        output |= input & 0x40000000;
        return output;
    }
    

}
