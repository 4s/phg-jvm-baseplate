
package dk.s4.phg.baseplate;

/**
 * The overall state of the application.
 *
 * This enumeration describes the current overall state of the
 * application. Modules receive this state using the
 * ModuleBase.onApplicationStateChange() method and may acquire and
 * release resources accordingly.
 *
 * WARNING: THE STATE MACHINE FOR MODULE SHUTDOWN IS
 * INCOMPLETE. EXPECT CHANGES!
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public enum ApplicationState {

    /**
     * The PHG Core baseplate is initializing
     *
     * The modules are born in this state and kept in this state until
     * all PHG Core modules have been started and successfully
     * connected to the baseplate.
     */
    INITIALIZING,

    /**
     * The PHG Core baseplate has finished initializing but the
     * application is not yet started.
     *
     * The PHG Core changes from INITIALIZING to READY when all
     * modules have been connected and initialized. When a module have
     * received the notification that the Core is ready, it SHALL be
     * ready to handle and respond to any incoming messages on all of
     * its interfaces. However, it SHALL NOT _initiate_ any
     * communication of any kind with other modules.
     *
     * The PHG Core baseplate guarantees that all modules have been
     * initialized before any module changes to the READY state, and
     * that all modules have changed to the READY state before any
     * module changes to the RUNNING state.
     */
    READY,

    /**
     * The application is started (or about to start).
     *
     * The PHG Core changes from READY to RUNNING when all modules
     * have moved to the READY state, which implies that they are
     * ready to handle and respond to messages on all interfaces. When
     * a module have received the notification that the Core is
     * running, it SHALL be ready to handle and respond to any
     * incoming messages on all of its interfaces and MAY _initiate_
     * communication with other modules.
     *
     * The PHG Core baseplate guarantees that all modules have changed
     * to the READY state before any module changes to the RUNNING
     * state and before the application starts communicating with any
     * PHG Core module.
     */
    RUNNING,

    /**
     * Application is about to shut down.
     *
     * WARNING: THE STATE MACHINE FOR MODULE SHUTDOWN IS
     * INCOMPLETE. EXPECT CHANGES!
     */
    SHUTDOWN_REQUEST,

    
    /**
     * Application is shutting down.
     *
     * No more communication will happen on the message bus. Please
     * release all acquired resources.
     *
     * WARNING: THE STATE MACHINE FOR MODULE SHUTDOWN IS
     * INCOMPLETE. EXPECT CHANGES!
     */
    FINALIZING
}
