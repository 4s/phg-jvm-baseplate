package dk.s4.phg.baseplate;

import dk.s4.phg.baseplate.internal.Header;
import dk.s4.phg.baseplate.internal.HeaderParseException;
import dk.s4.phg.baseplate.internal.PayloadParseException;
import dk.s4.phg.baseplate.internal.MessageFormatException;
import dk.s4.phg.baseplate.internal.MessagingException;
import dk.s4.phg.baseplate.internal.UnexpectedMessagingException;
import dk.s4.phg.messages.classes.error.Error;
import dk.s4.phg.messages.classes.log.Log;
import dk.s4.phg.messages.classes.log.LogLevel;

import com.google.protobuf.MessageLite;
import com.google.protobuf.InvalidProtocolBufferException;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;
import org.zeromq.ZThread;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.lang.reflect.InvocationTargetException;




/**
 * PHG Core module abstract base class.
 *
 * All PHG Core modules must expose an implementation of this abstract
 * class.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public abstract class ModuleBase {

    /**
     * The PHG Core context. Package-private access as it is directly
     * accessed by other classes in this package.
     */
    final Context context;

    /**
     * Local "shadow" ZeroMQ context copy managing the ZeroMQ sockets
     * owned and accessed only by the module thread.
     */
    private final ZContext zcontext;

    /**
     * The socket for incoming data from the baseplate, owned and
     * accessed only by the module thread
     */
    private final ZMQ.Socket inSocket;

    /**
     * The socket for outgoing data from the baseplate, owned and
     * accessed only by the module thread
     */
    private final ZMQ.Socket outSocket;
    
    /**
     * The module thread. Everything in this class, except
     * construction (including EndpointInterface construction) is
     * supposed to run on this thread exclusively.
     */
    private final ModuleThread moduleThread;
    
    /**
     * The mandatory name of this module used when a log or error
     * message is recorded. Also known as the TAG. A module named "Foo
     * bar-baz module" should use the PascalCase equivalent
     * "FooBarBaz" as module_tag.
     */
    private final String moduleTag;

    /**
     * The Peer identity of this module. Used as source/destination
     * address on the communication bus.
     */
    private final Peer identity;

    /**
     * Queue of actions scheduled using execute() to run on the module
     * thread. The actionQueue object also serves as the lock to
     * access the actionQ_ext ZeroMQ socket.
     */
    private final ConcurrentLinkedQueue<Runnable> actionQueue
        = new ConcurrentLinkedQueue<Runnable>();

    /**
     * Internal (module thread) end of the pair of PAIR sockets used
     * to wake up the module thread when an action is scheduled using
     * execute()
     */
    private final ZMQ.Socket actionQ_int;

    /**
     * External (non-module-thread) end of the pair of PAIR sockets
     * used to wake up the module thread when an action is scheduled
     * using execute(). When the module thread exits, it will close
     * the socket and set actionQ_ext to null.
     */
    private ZMQ.Socket actionQ_ext;

    /**
     * The poller used to wait for input data for the module thread.
     */
    private final ZMQ.Poller poller;
    
    /**
     * Counter for generating unique ObjId objects. Owned and accessed
     * only by the module thread.
     */
    private int oidCounterMSB;
    private int oidCounterLSB;

    /**
     * The current state of the application as reported by the master
     * baseplate.
     */
    private ApplicationState applicationState
        = ApplicationState.INITIALIZING;

    /**
     * Log message Headers are prepared in advance and cached here,
     * rather than building them each time.
     * For each LogLevel a separate header exists.
     */
    private final Header logHeader[] = new Header[5];

    /**
     * Allow creation of new InterfaceEndpoints
     *
     * Creating InterfaceEndpoints is only allowed before the module
     * thread is started - which is usually the last thing the
     * most-derived class will do in its constructor.
     */
    private boolean initializingEndpoints;

    /**
     * Map of the registered InterfaceEndpoints, identifiable by their
     * subject
     */
    private Map<String, InterfaceEndpoint> receiverInterfaces
        = new HashMap<String, InterfaceEndpoint>();

    /**
     * The next available callback ID.
     */
    private short callbackIdCounter = 0;

    /**
     * Struct holding a Callback along with its dynamic type
     */
    private class CallbackAndType<R extends MessageLite> {
        Callback<? super R> callback;
        Class<R> type;
        void success(MessageLite retVal, boolean lastCall) {
            callback.success(type.cast(retVal), lastCall);
        }
    }

    /**
     * Map of the registered Callbacks, identifiable by their callback
     * id.
     */
    private Map<Short, CallbackAndType<?>> callbacks
        = new HashMap<Short, CallbackAndType<?>>();

    /**
     * List of the callbacks registered to receive data from worker
     * threads.
     */
    private ArrayList<BiConsumer<byte[], Boolean>> workerCallbacks
        = new ArrayList<BiConsumer<byte[], Boolean>>();

    /**
     * List of the sockets receiving data from worker threads.
     */
    private ArrayList<ZMQ.Socket> workerSockets
        = new ArrayList<ZMQ.Socket>();

    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*                         Constructor                          */
    /*                                                              */
    /* ************************************************************ */


    /**
     * Create the new module's base instance.
     *
     * The derived class's constructor should proceed to register all
     * interfaces using the <code>implements[*]Interface</code>
     * methods, and finally call start() to signal the end of the
     * construction of the module.
     *
     * @param context   The PHG Core common Context object shared by all
     *                  modules connected to the JVM baseplate
     * @param moduleTag The mandatory name of this module used when a
     *                  log or error message is recorded. Also known
     *                  as the TAG. A module named "Foo bar-baz module"
     *                  should use the PascalCase equivalent
     *                  "FooBarBaz" as module_tag
     * @throws zmq.ZError.CtxTerminatedException If the context was
     *                                           closed
     */
    protected ModuleBase(Context context, String moduleTag) {
        this.context = context;
        this.moduleTag = moduleTag;

        // Initialise the ObjId generator counter
        oidCounterMSB = context.generateUID();
        oidCounterLSB = 0;

        // The Peer is generated based on re-using the first
        // generateUID call made from this module.
        identity = Peer.fromInt32(oidCounterMSB);

        // Create the ZMQ shadow context for this module
        zcontext = ZContext.shadow(context.getZMQContext());

        // ... and the poller used to receive data
        // (The implementation uses an ArrayList of initial size 5
        // which should be enough for most cases)
        poller = zcontext.createPoller(5);
        
        // Create and connect the two sockets for communication
        // - the PUB socket
        outSocket = zcontext.createSocket(SocketType.PUB);
        outSocket.connect(context.getModule2reflector());
        // - the SUB socket
        inSocket = zcontext.createSocket(SocketType.SUB);
        inSocket.connect(context.getReflector2module());
        // Subscribe all broadcast and unicast topics (Broadcast is
        // subscribed immediately, unicast is postponed to later)
        inSocket.subscribe(Header.topicBroadcast());
        subscribe(Header.topicUnicast(identity));

        // Create a pair of PAIR sockets for action queue
        // notifications - using a pseudo-random unique string as
        // socket URI
        String pairUri = String.format("inproc://actionQ-%d", hashCode());
        actionQ_ext = zcontext.createSocket(SocketType.PAIR);
        actionQ_ext.bind(pairUri);
        actionQ_int = zcontext.createSocket(SocketType.PAIR);
        actionQ_int.connect(pairUri);
        
        // Create the module thread, but don't start it yet
        moduleThread = new ModuleThread("ModuleThread of the " + moduleTag
                                        + " module");

        // Create the log message headers
        // Different log levels use different signal values
        logHeader[0] = Header.systemMulticast(identity, "Log", "");     // DEBUG
        logHeader[1] = Header.systemMulticast(identity, "Log", "+");    // INFO
        logHeader[2] = Header.systemMulticast(identity, "Log", "++");   // WARN
        logHeader[3] = Header.systemMulticast(identity, "Log", "+++");  // ERROR
        logHeader[4] = Header.systemMulticast(identity, "Log", "++++"); // FATAL

        // Initializing endpoints is now allowed
        initializingEndpoints = true;
    }
    
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*         Public identity and getters of this module           */
    /*                                                              */
    /* ************************************************************ */

    
    /**
     * Get the Peer identity of this module.
     *
     * The Peer identity of this module is used as source/destination
     * address on the communication bus.
     *
     * @return The Peer object representing this module
     */
    public Peer getId() {
        return identity;
    }

    /**
     * Get the module TAG used for identifying this module in logging
     * and error messages.
     *
     * The name of this module used when a log or error message is
     * recorded. Also known as the TAG. A module named "Foo bar-baz"
     * should use the PascalCase equivalent "FooBarBaz" as moduleTag.
     *
     * @return The module TAG
     */
    public String getModuleTag() {
        return moduleTag;
    }

    /**
     * Get the current application state.
     *
     * This will return the current state of the application as
     * reported by the MasterModule. The value will always be
     * identical to the last reported value from
     * onApplicationStateChange() - and INITIALIZING at the beginning
     * before any state change was reported.
     * @return The current application state.
     * @see ApplicationState
     */
    public ApplicationState getApplicationState() {
        return applicationState;
    }

    /**
     * Create a fresh ObjId.
     *
     * Objects shared across modules are individually identified by an
     * ObjId object, which is a system-wide unique identifier created
     * and maintained by the baseplate and transported in Google
     * Protobuf messages as a single 64-bit integer.
     *
     * This method is not thread safe and shall always be called by
     * the module thread.
     *
     * @return The newly created ObjID
     */
    public ObjId createObjId() {

        // Combine the 32-bit MSB and LSB parts of the counter into a
        // single 64-bit integer
        long oidCounter = oidCounterMSB;
        oidCounter <<= 32;
        oidCounter |= oidCounterLSB;

        // Increment the LSB part of the counter
        oidCounterLSB ++;
        // Carry to the MSB will require a fresh value
        if (oidCounterLSB == 0) {
            // Get a fresh MSB value from the baseplate
            oidCounterMSB = context.generateUID();
        }

        // return the ObjId object wrapping oidCounter
        return ObjId.fromInt64(oidCounter);
    }
    
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*  Communication between this abstract class and its subclass  */
    /*                                                              */
    /* ************************************************************ */

    
    /**
     * Start the module.
     *
     * This method is used to start the module after all interfaces
     * have been registered. The recommended way to use this method is
     * as the last statement of the most derived class's constructor,
     * assuming that the derived constructor(s) registers all
     * module interfaces.
     *
     * After invoking this method, the creation of new
     * InterfaceEndpoints for this module is no longer allowed. The
     * implements{Producer|Consumer}Interface() methods will throw
     * IllegalStateException if that is attempted.
     */
    protected void start() {

        // Initializing endpoints is no longer allowed
        initializingEndpoints = false;

        // Start the module thread
        moduleThread.start();
    }

    /**
     * The application state has changed.
     *
     * Subclasses may override this method to intercept and act on
     * changes to the overall application state. The application state
     * is maintained by the MasterModule and will be broadcast to all
     * modules.
     * @param newState The state the application is transitioning to.
     * @see ApplicationState
     */
    protected void onApplicationStateChange(ApplicationState newState) {}

    /**
     * Create a new InterfaceEndpoint for a producer of the interface
     * defined by interfaceName.
     *
     * This operation is only allowed before the start() method is
     * invoked, which normally means within the constructor of a
     * derived class. An IllegalStateException will be thrown
     * otherwise.
     *
     * @param interfaceName The name of he interface, we are
     *                      implementing a producer for using
     *                      PascalCase formatting.
     * @return The newly constructed InterfaceEndpoint
     * @throws IllegalStateException if this method was called after
     *         the module was started, or if another InterfaceEndpoint
     *         was already registered for the same interface on the
     *         same module. We cannot allow that.
     * @throws IllegalArgumentException if interfaceName is null or
     *         the interface could not be found.
     */
    protected InterfaceEndpoint
        implementsProducerInterface(String interfaceName) {
        if (!initializingEndpoints) {
            throw new IllegalStateException("Module already started");
        }
        return new InterfaceEndpoint(this, interfaceName, true);
    }
    
    /**
     * Create a new InterfaceEndpoint for a consumer of the interface
     * defined by interfaceName.
     *
     * This operation is only allowed before the start() method is
     * invoked, which normally means within the constructor of a
     * derived class. An IllegalStateException will be thrown
     * otherwise.
     *
     * @param interfaceName The name of he interface, we are
     *                      implementing a consumer for using
     *                      PascalCase formatting
     * @return The newly constructed InterfaceEndpoint
     * @throws IllegalStateException if this method was called after
     *         the module was started, or if another InterfaceEndpoint
     *         was already registered for the same interface on the
     *         same module. We cannot allow that.
     * @throws IllegalArgumentException if interfaceName is null or
     *         the interface could not be found.
     */
    protected InterfaceEndpoint
        implementsConsumerInterface(String interfaceName) {
        if (!initializingEndpoints) {
            throw new IllegalStateException("Module already started");
        }
        return new InterfaceEndpoint(this, interfaceName, false);
    }

    /**
     * The EndpointFactory interface.
     *
     * This interface combined with the endpointFactory() method may
     * be used by the module constructor to delegate the declaration
     * and implementation of an interface to a different class. The
     * <code>implements[x]Interface</code> are otherwise protected as
     * no public access to these methods should be allowed.
     *
     * A class implementing an interface may accept an
     * EndpointInterface as an argument to its constructor and MUST
     * ONLY call the <code>implements[x]Interface</code> methods in
     * its own constructor! It is not allowed to keep a copy of this
     * factory object for use outside the constructor.
     */
    public interface EndpointFactory {
        /**
         * Create a new InterfaceEndpoint for a producer of the
         * interface defined by interfaceName.
         *
         * This operation is only allowed before the start() method is
         * invoked, which normally means within the constructor of a
         * module. An IllegalStateException will be thrown otherwise.
         *
         * @param interfaceName The name of he interface, we are
         *                      implementing a producer for
         * @return The newly constructed InterfaceEndpoint
         * @throws IllegalStateException if this method was called
         *         after the module was started, or if another
         *         InterfaceEndpoint was already registered for the
         *         same interface on the same module. We cannot allow
         *         that.
         */
        InterfaceEndpoint
            implementsProducerInterface(String interfaceName);
        
        /**
         * Create a new InterfaceEndpoint for a consumer of the
         * interface defined by interfaceName.
         *
         * This operation is only allowed before the start() method is
         * invoked, which normally means within the constructor of a
         * module. An IllegalStateException will be thrown otherwise.
         *
         * @param interfaceName The name of he interface, we are
         *                      implementing a consumer for
         * @return The newly constructed InterfaceEndpoint
         * @throws IllegalStateException if this method was called
         *         after the module was started, or if another
         *         InterfaceEndpoint was already registered for the
         *         same interface on the same module. We cannot allow
         *         that.
         */
        InterfaceEndpoint
            implementsConsumerInterface(String interfaceName);
    }

    /**
     * Return an EndpointFactory object.
     *
     * The EndpointFactory forwards method invocations to the
     * otherwise protected methods on the ModuleBase class.
     * @return The EndpointFactory
     * @see EndpointFactory
     */
    protected EndpointFactory endpointFactory() {
        return new EndpointFactory() {
            public InterfaceEndpoint
                implementsProducerInterface(String interfaceName) {
                return ModuleBase.this
                    .implementsProducerInterface(interfaceName);
            }
            public InterfaceEndpoint
                implementsConsumerInterface(String interfaceName) {
                return ModuleBase.this
                    .implementsConsumerInterface(interfaceName);
            }
        };
    }

    /**
     * Enqueue an action for execution on the module thread.
     *
     * This method is a Java extension to the ModuleBase which can be
     * used to schedule a runnable to be executed on the module thread
     * from other threads. This offers a simpler thread-safe
     * multi-thread API than a full WorkerThread, when this is not
     * needed.
     *
     * @param action The action to execute
     * @throws IllegalArgumentException if the action is null
     */
    protected void enqueueAction(Runnable action) {

        if (action == null) {
            throw new IllegalArgumentException("The action cannot be null");
        }
        
        synchronized(actionQueue) {
            // Module thread still running?
            if (actionQ_ext != null) {
            
                // Add the task to the execute queue
                actionQueue.add(action);
                
                // Send notification to module thread
                actionQ_ext.send(new byte[] {0}, 0);
            }
        }
    }

    /**
     * Returns a Consumer object wrapping the enqueueAction method.
     *
     * This method is a Java extension to the ModuleBase which offers
     * a simple way to let other classes of the module implementation
     * enqueue actions on the module thread.
     *
     * @return A Consumer which accepts Runnable actions and enqueues
     *         them in a thread-safe manner to be executed on the
     *         module thread.
     */
    protected Consumer<Runnable> enqueueActionConsumer() {
        return new Consumer<Runnable>() {
            public void accept(Runnable action) {
                enqueueAction(action);
            }
        };
    }
    
    /**
     * A worker to be executed on a separate thread.
     *
     * Worker threads are used by platform-dependent modules that need
     * to call blocking platform APIs, as the module's own thread is
     * not allowed to perform blocking calls.
     *
     * A worker thread is created using the fork()
     * method. Communication between the main module thread and the
     * worker thread takes place using two(!) pairs of PAIR
     * sockets. This is the main difference when compared to the
     * org.zeromq.ZThread class.
     *
     * Using two pairs of sockets instead of one will allow the worker
     * thread to delegate the txPipe to another thread (or by proper
     * synchronization guards a pool of threads), such as incoming
     * data from external sources, e.g. operating system I/O.
     *
     * The thread shall terminate when the main module thread closes
     * the two pipes. The thread may detect this by catching the
     * ZMQException(ETERM) when polling the rxPipe. At this point it
     * must free all resources and return. If the WorkerRunnable.run()
     * method throws any other RuntimeException except
     * ZMQException(ETERM) out, the exception will be logged as a
     * "FATAL" level log message, and future attempts to send messages
     * from the main module thread to the worker will result in an
     * IllegalStateException
     */
    public interface WorkerRunnable {
        /**
         * The main method of the worker thread
         *
         * The worker thread will terminate when this method
         * returns. Normal termination of the worker thread also
         * includes throwing the ZMQException(ETERM). Any other
         * RuntimeExceptions will be logged as abnormal termination.
         *
         * @param args   The vararg arguments provided by fork
         * @param ctx    A shadow ZContext created for this particular
         *               worker thread
         * @param rxPipe A PAIR socket for communication from the main
         *               module thread TO the worker thread. It cannot
         *               be used in the opposite direction.
         * @param txPipe A PAIR socket for communication from the
         *               worker thread TO the main module thread. It
         *               cannot be used in the opposite direction.
         */
        void run(Object[] args, ZContext ctx, ZMQ.Socket rxPipe,
                 ZMQ.Socket txPipe);
    }

    /**
     * Create and start a worker thread.
     *
     * Worker threads are used by platform-dependent modules that need
     * to call blocking platform APIs, as the module's own thread is
     * not allowed to perform blocking calls.
     *
     * The worker thread implementation is wrapped in the
     * WorkerRunnable interface. This method must be called by the
     * main module thread and will result in the creation of a thread
     * with which the main module thread can communicate using the two
     * BiConsumers.
     *
     * The BiConsumers take a single message fragment as a byte array
     * along with a boolean indicating if more message fragments will
     * follow belonging to the same message.
     *
     * @see            WorkerRunnable for details
     * @param callback This callback is invoked on the main module
     *                 thread with message fragments received from the
     *                 worker thread's txPipe.
     * @param runnable The worker thread main method
     * @param arguments Arguments for the runnable
     * @return         The main module thread uses this BiConsumer to
     *                 send message fragments to the worker thread's
     *                 rxPipe. If the module thread has already
     *                 terminated, an IllegalStateException will be
     *                 thrown.
     */
    protected BiConsumer<byte[], Boolean>
        fork(BiConsumer<byte[], Boolean> callback,
             WorkerRunnable runnable, Object... arguments) {

        // Open and bind the module thread end of the second "tx" pipe
        ZMQ.Socket txPipeModule = zcontext.createSocket(SocketType.PAIR);
        String pairUri = String.format("inproc://pair-%d",
                                       txPipeModule.hashCode());
        txPipeModule.bind(pairUri);

        // Wrap the worker with the BiConsumer sending to it.
        WorkerWrapper wrapper = new WorkerWrapper(runnable, pairUri);
        
        // Create and start the thread
        wrapper.rxPipeModule = zcontext.fork(wrapper, arguments);
        
        // Register the callback argument as handler of the
        // txPipeModule input

        poller.register(txPipeModule, ZMQ.Poller.POLLIN);
        workerSockets.add(txPipeModule);
        workerCallbacks.add(callback);
        
        // And return the wrapper which also implements the BiConsumer
        // for sending data to the worker thread
        return wrapper;
    }

    /**
     * This private class wraps the worker and the bi-consumer sending
     * to it. These two needs to be linked because the (premature)
     * termination of the worker thread must be communicated back to
     * the module thread - if this happens, the BiConsumer accept()
     * implementation will throw an IllegalStateException.
     */
    private class WorkerWrapper implements BiConsumer<byte[], Boolean>,
                                           ZThread.IAttachedRunnable {

        WorkerRunnable runnable;
        ZMQ.Socket rxPipeModule;
        String pairUri;
        
        // This volatile boolean is responsible for communicating the
        // termination of the worker thread back to the module thread.
        volatile boolean closed;
        
        WorkerWrapper(WorkerRunnable runnable, String pairUri) {
            this.runnable = runnable;
            this.pairUri = pairUri;
            closed = false;
        } 

        // The run() method is executed on the newly-created worker
        // thread.
        @Override
        public void run(Object[] args, ZContext ctx,
                        ZMQ.Socket rxPipeWorker) {

            // Open and connect the second "tx" Pipe
            ZMQ.Socket txPipeWorker
                = ctx.createSocket(SocketType.PAIR);
            txPipeWorker.connect(pairUri);

            // Execute the runnable
            try {
                runnable.run(args, ctx, rxPipeWorker,
                             txPipeWorker);
            } catch (ZMQException e) {
                if (e.getErrorCode()
                    != ZMQ.Error.ETERM.getCode()) {
                    logger.fatal("WorkerRunnable unexpected exit",
                                 e);
                }
            } catch (RuntimeException e) {
                logger.fatal("WorkerRunnable unexpected exit", e);
            } finally {
                closed = true; 
            }
        }
        
        // BiConsumer for sending messages through rxPipeModule. This
        // method is executed on the module thread.
        @Override
        public void accept(byte[] buffer, Boolean hasMore) {
            
            // If the socket was closed at the other end,
            // throw an IllegalStateException
            if (closed) {
                throw new IllegalStateException
                    ("Attempt to send message to terminated WorkerRunnable");
            }

            // Send the buffer to the rxPipeModule
            rxPipeModule.send(buffer, hasMore ?
                              ZMQ.SNDMORE : 0);
        }
    }
    
    /**
     * Module task loop is starting.
     *
     * Subclasses may override this method to inject work at the
     * beginning of the task loop (on the module Thread).
     */
    protected void initialize() {}
    
    /**
     * Module task loop is finishing.
     *
     * Subclasses may override this method to inject work when the
     * task loop shuts down. This method will be executed at the very
     * end just before the Thread terminates.
     */
    protected void cleanup() {}
    
    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*             Internal module thread implementation            */
    /*                                                              */
    /* ************************************************************ */
    
    /**
     * The module thread task loop
     */
    private class ModuleThread extends Thread {

        private ModuleThread(String name) { super(name); }
        
        @Override
        public void run() {

            try {
                
                poller.register(inSocket, ZMQ.Poller.POLLIN);
                poller.register(actionQ_int, ZMQ.Poller.POLLIN);
                
                // Initialize subclass on the module thread
                initialize();
                
                // Task loop
                while (true) {
                    if (actionQueue.isEmpty() && poller.poll() < 1) {
                        if (inSocket.errno() != 0) {
                            // If the inSocket is in some error state,
                            // it does not make sense to continue
                            throw new org.zeromq.ZMQException
                                (inSocket.errno());
                        }
                        continue;
                    }

                    do {
                        // This "loop" is always executed once, but we
                        // need to be able to "break" out.
                    
                        // Dispatch input from the baseplate (inSocket)
                        if (poller.pollin(0)) {
                    
                            // We did receive a message from the baseplate
                            
                            ZMQ.Socket in = inSocket;
                            
                            // Receive the first fragment
                            byte[] rawHeader = in.recv(ZMQ.DONTWAIT);

                            if (rawHeader == null) {
                                // Due to the nature of the main loop
                                // we may have already processed the
                                // input, in which case EAGAIN will be
                                // reported. Other error codes are not
                                // expected.
                                if (in.errno() != ZMQ.Error.EAGAIN.getCode()) {
                                    // Unexpected error from ZeroMQ: CorePanic
                                    context.corePanic(moduleTag,
                                                      new ZMQException
                                                      (in.errno()), false);
                                }
                                break;
                            }
                        
                            // Receive the second fragment
                            byte[] payload = null;
                            if (in.hasReceiveMore()) {
                                payload = in.recv();
                            }

                            // More fragments? Should never happen. We
                            // invoke panic and attempt to recover by
                            // flushing the message and starting over
                            if (in.hasReceiveMore()) {
                                context.corePanic
                                    (moduleTag, new MessageFormatException
                                     ("Too many message fragments (>2)", null),
                                     false);
                                while (in.hasReceiveMore()) { in.recv(); }
                                break;
                            }

                            // See if a default handler will handle the message
                            if (defaultHandler(rawHeader, payload)) break;
                        
                            // Parse the header
                            Header header;
                            try {
                                header = Header.parse(rawHeader);
                            } catch (HeaderParseException e) {
                                // Should never happen. We invoke panic
                                // and attempt to recover by starting over
                                context.corePanic(moduleTag, e, false);
                                break;
                            }
                        
                            // Sanity check - should never happen. We
                            // invoke panic and attempt to recover by
                            // starting over
                            if (header.mode == Header.AddressingMode.UNICAST &&
                                !header.receiver.equals(identity)) {
                                context.corePanic
                                    (moduleTag, new UnexpectedMessagingException
                                     ("Wrong unicast receiver", null), false);
                                break;
                            }

                            // See if a default handler will handle the message
                            if (defaultHandler(header, payload)) break;

                        
                            // Standard dispatcher begins here...
                        
                            // Handle callback-type messages
                            if (header.isCallback()) {
                            
                                // Lookup the callbackInvokeId to locate
                                // the handler and payload type
                                CallbackAndType<?> cat =
                                    callbacks.get(header.callbackInvokeId);

                                // Not found: corePanic and recover
                                if (cat == null) {
                                    context.corePanic
                                        (moduleTag,
                                         new UnexpectedMessagingException
                                         ("Unexpected callback message", null),
                                         false);
                                    break;
                                }
                            
                                // Test if payload is null

                                if (payload == null) {
                                    // Payload is null. This is a core
                                    // system error that should never
                                    // happen. We panic...
                                    context.corePanic
                                        (moduleTag,
                                         new UnexpectedMessagingException
                                         ("Payload is null", null), false);
                                    break;
                                }

                                // Create MetaData
                                MetaData meta = new MetaData(header);

                                // Parse the payload and invoke callback
                                if (header.isError()) {

                                    // Payload should be of type Error
                                    Error err;
                                    try {
                                        err = Error.parseFrom(payload);
                                    } catch (InvalidProtocolBufferException e) {
                                        context.corePanic
                                            (moduleTag,
                                             new PayloadParseException
                                             ("Error parsing incoming payload "
                                              + "as an Error message.", e),
                                             false);
                                        break;
                                    }

                                    // Invoke callback
                                    cat.callback.error(CoreError.fromError(err), meta.more());

                                } else {
                            
                                    // Parse the payload of the given cat.type
                                    MessageLite msg;
                                    try {
                                        msg = (MessageLite)cat.type.getMethod
                                            ("parseFrom", byte[].class)
                                            .invoke(null, payload);
                                    } catch (InvocationTargetException e) {
                                        context.corePanic
                                            (moduleTag,
                                             new PayloadParseException
                                             ("Error parsing incoming payload as a "
                                              + cat.type.getName() + " message.", e),
                                             false);
                                        break;
                                    } catch (ReflectiveOperationException e) {
                                        context.corePanic
                                            (moduleTag,
                                             new UnexpectedMessagingException
                                             ("Unexpected", e), false);
                                        break;
                                    }
                                
                                    // Invoke callback
                                    cat.success(msg, meta.more());
                                }
                            
                                // If last callback, delete the stored callback info
                                if (!meta.more()) {
                                    callbacks.remove(header.callbackInvokeId);
                                }
                            
                                break;
                            }

                            // Handle interface-type messages
                            boolean c2p = header.isC2P();
                            if (header.isP2C() || c2p) {

                                // Lookup the subject to find the endpoint
                                InterfaceEndpoint endpoint = 
                                    receiverInterfaces.get(header.subject);

                                // InterfaceEndpoint not registered
                                if (endpoint == null) {

                                    if (header.mode
                                        == Header.AddressingMode.UNICAST) {
                                        // For unicast messages, this is
                                        // probably an error at the sender
                                        // module. We log an error and
                                        // continue...

                                        logger.error("Unicast message received " +
                                                     "for unknown interface: " +
                                                     header.subject);
                                    
                                    } else {
                                        // For multicast messages this is
                                        // a core system error that should
                                        // never happen. We panic...
                                
                                        context.corePanic
                                            (moduleTag,
                                             new UnexpectedMessagingException
                                             ("Multicast message received " +
                                              "for unknown interface: " +
                                              header.subject, null), false);
                                    }
                                    break;
                                }
                            
                                if (payload == null) {
                                    // Payload is null. This is a core
                                    // system error that should never
                                    // happen. We panic...
                                    context.corePanic
                                        (moduleTag,
                                         new UnexpectedMessagingException
                                         ("Payload is null", null), false);
                                    break;
                                }

                                try {
                                    // Delegate to the interfaceEndpoint
                                    endpoint.messageReceived(payload, header);
                                } catch (MessagingException e) {
                                    // Exception thrown out of the
                                    // handler. corePanic
                                    context.corePanic(getModuleTag(), e, true);
                                    // We are not sure about the state of the
                                    // module, so we bail out.
                                    break;
                                }
                            
                                break;
                            }

                        
                            // At this point all message types with
                            // payload have been processed. Only system
                            // messages comprising a single fragment
                            // remains. (System-type multicast messages
                            // are handled by the defaultHandler)

                            // If payload is not null, we invoke panic and
                            // attempt to recover by starting over
                            if (payload != null) {
                                context.corePanic(moduleTag,
                                                  new MessageFormatException
                                                  ("Unexpected message payload",
                                                   null), false);
                                break;
                            }
                        
                        
                            if (header.mode == Header.AddressingMode.BROADCAST) {

                                // System Broadcast message

                                // FIXME
                                logger.warn("Unhandled System broadcast");
                            
                            } else if (header.mode ==
                                       Header.AddressingMode.UNICAST) {

                                // System Unicast message

                                // FIXME
                                logger.warn("Unhandled System unicast");

                            } else {

                                // This should not be possible. We
                                // invoke panic and attempt to recover by
                                // starting over
                                context.corePanic
                                    (moduleTag, new UnexpectedMessagingException
                                     ("Unexpected multicast message", null),
                                     false);
                            }
                        }
                    } while (false);

                    // Flush the action queue signalling pipe
                    if (poller.pollin(1)) {
                        // Flush the pipe
                        actionQ_int.recv(ZMQ.DONTWAIT);
                    }

                    // Execute action on the action queue (actionQ_int)
                    Runnable action = actionQueue.poll();
                    if (action != null) action.run();
                    
                    // Iterate through messages from registered worker threads
                    for (int i = 2; i < poller.getSize(); i++) {
                        if (poller.pollin(i)) {
                            BiConsumer<byte[], Boolean> callback
                                = workerCallbacks.get(i-2);
                            ZMQ.Socket socket = workerSockets.get(i-2);

                            // First message fragment
                            byte[] msg = socket.recv(ZMQ.DONTWAIT);
                            if (msg != null) {
                                // Loop through remaining message fragments
                                while (socket.hasReceiveMore()) {
                                    callback.accept(msg, socket.hasReceiveMore());
                                    msg = socket.recv();
                                }
                                callback.accept(msg, socket.hasReceiveMore());
                            } else if (socket.errno() !=
                                       ZMQ.Error.EAGAIN.getCode()) {
                                // Due to the nature of the main loop
                                // we may have already processed the
                                // input, in which case EAGAIN will be
                                // reported. Other error codes are not
                                // expected: CorePanic
                                context.corePanic(moduleTag,
                                                  new ZMQException
                                                  (socket.errno()), false);
                            }
                        }
                    }
                }
            } catch (RuntimeException e) {
                // Unexpected termination due to unhandled exception
                // Report it
                context.corePanic(moduleTag, e, true);
            } finally {

                try {
                    // Subclasses may inject code at the end
                    cleanup();
                } catch (RuntimeException e) {
                    // Report exception thrown out of the finalizer
                    context.corePanic(getModuleTag(), e, true);
                }

                // Free all resources
                poller.close();

                actionQ_int.close();
                synchronized(actionQueue) {
                    actionQ_ext.close();
                    actionQ_ext = null;
                }

                outSocket.close();
                inSocket.close();

                zcontext.destroy();
            }
        }
    }

        


    /* ************************************************************ */
    /*                                                              */
    /*             Interface to other baseplate classes             */
    /*                                                              */
    /* ************************************************************ */

    
    /**
     * Register a callback to handle the response(s) of a function
     * request.
     *
     * @param <T> The type of a success response
     * @param callback the callback handler
     * @param type of the success response as a Class object
     * @return The assigned callbackId
     * @throws UnexpectedMessagingException If no callbackId was
     *         available - most probably a memory leak
     */
    <T extends MessageLite>
    short registerCallback(Callback<? super T> callback, Class<T> type)
                              throws UnexpectedMessagingException {
        // Pick the next callbackId..
        short callbackDeclId = callbackIdCounter++;
        // .. and check if it is already in use
        if (callbacks.containsKey(callbackDeclId)) {
            // If so pick the next instead
            callbackDeclId = callbackIdCounter++;
            // Extremely unlikely that two consecutive ids are used
            if (callbacks.containsKey(callbackDeclId)) {
                callbackDeclId = callbackIdCounter++;
                // Two-in-a-row is extremely unlikely. Three is
                // virtually impossible, unless we have a serious
                // memory leak...
                if (callbacks.containsKey(callbackDeclId)) {
                    throw new UnexpectedMessagingException
                        ("Heavy callback id reuse. Memory leaking.",null);
                }
            }
        }
        
        // Store the callback in the callbacks list
        CallbackAndType<T> cat = new CallbackAndType<T>();
        cat.callback = callback;
        cat.type = type;
        callbacks.put(callbackDeclId, cat);

        // And return the id
        return callbackDeclId;
    }

    /**
     * Add an InterfaceEndpoint handling a subject. Each subject can
     * only be registered once.
     * @param subject The subject to handle
     * @param endpoint The endpoint handling this subject
     * @return true If the endpoint was successfully registered. false
     *              if a handler was already registered for this subject
     */
    boolean addReceiverInterface(String subject,
                                 InterfaceEndpoint endpoint) {
        // Test if the subject is already registered.
        if (receiverInterfaces.containsKey(subject)) {
            // Already registered.
            return false;
        } else {
            // This is the first time
            receiverInterfaces.put(subject,endpoint);
            subscribe(Header.topicMulticast(subject));
        }
        return true;
    }
    
    /**
     * Default handler (raw header)
     *
     * Used by various baseplate module classes to intercept messages
     * from the baseplate before the standard dispatcher.
     *
     * Normal modules should not need this method.
     *
     * @param header The raw header fragment
     * @param payload The payload fragment (or null if the message
     *                only comprise one fragment).
     * @returm true if the handler did handle the message.
     */
    boolean defaultHandler(byte[] header, byte[] payload) { return false; }

    /**
     * Default handler (parsed header)
     *
     * Used by various baseplate module classes to intercept messages
     * from the baseplate before the standard dispatcher.
     *
     * Normal modules should not need this method.
     *
     * @param header The header fragment parsed as a Header
     * @param payload The payload fragment (or null if the message
     *                only comprise one fragment).
     * @return true if the handler did handle the message.
     */
    boolean defaultHandler(Header header, byte[] payload) { return false; }

    /**
     * Send a message to the baseplate.
     *
     * A message comprises a mandatory header fragment and a
     * conditional payload fragment.
     * @param header The mandatory header
     * @param payload The optional header (may be null)
     */
    void sendMessage(Header header, MessageLite payload) {
        if (payload == null) {
            outSocket.send(header.serialized, 0);
        } else {
            outSocket.send(header.serialized, ZMQ.SNDMORE);
            outSocket.send(payload.toByteArray(), 0);
        }
    }

    /**
     * Send a (raw) message to the baseplate.
     *
     * A message comprises a mandatory header fragment and a
     * conditional payload fragment.
     * @param header The mandatory header
     * @param payload The optional header (may be null)
     */
    void sendMessage(byte[] header, byte[] payload) {
        if (payload == null) {
            outSocket.send(header, 0);
        } else {
            outSocket.send(header, ZMQ.SNDMORE);
            outSocket.send(payload, 0);
        }
    }


    //
    // FIXME: Subscribe/unsubscribe during initialization is not
    // allowed. In the INITIALIZING state only "B_" subscription is
    // allowed. Instead a list of topics must be constructed and when
    // the application state advances from INITIALIZING all the
    // pending subscriptions must be performed.
    //
    
    /**
     * Add a subscription from the baseplate
     * @param topic The topic (header prefix) we wish to subscribe to.
     */
    void subscribe(byte[] topic) {
        inSocket.subscribe(topic);
    }
    
    /**
     * Cancel a subscription from the baseplate
     * @param topic The topic (header prefix) we wish to stop subscribing
     * to.
     */
    void unsubscribe(byte[] topic) {
        inSocket.unsubscribe(topic);
    }



    
    /* ************************************************************ */
    /*                                                              */
    /*                      Logging and errors                      */
    /*                                                              */
    /* ************************************************************ */


    /**
     * Implementation of the Logger interface
     */
    private Logger logger = new Logger() {

            @Override
            public void fatal(String message) {
                loggerOutput(LogLevel.FATAL, message, null, "fatal");
            }
    
            @Override
            public void fatal(String message, CoreError error) {
                loggerOutput(LogLevel.FATAL, message, error, "fatal");
            }
    
            @Override
            public void fatal(String message, Throwable thr) {
                loggerOutput(LogLevel.FATAL, message, "fatal", thr);
            }
    
            @Override
            public void error(String message) {
                loggerOutput(LogLevel.ERROR, message, null, "error");
            }
    
            @Override
            public void error(String message, CoreError error) {
                loggerOutput(LogLevel.ERROR, message, error, "error");
            }
    
            @Override
            public void error(String message, Throwable thr) {
                loggerOutput(LogLevel.ERROR, message, "error", thr);
            }
    
            @Override
            public void warn(String message) {
                loggerOutput(LogLevel.WARN, message, null, "warn");
            }

            @Override
            public void warn(String message, CoreError error) {
                loggerOutput(LogLevel.WARN, message, error, "warn");
            }

            @Override
            public void warn(String message, Throwable thr) {
                loggerOutput(LogLevel.WARN, message, "warn", thr);
            }

            @Override
            public void info(String message) {
                loggerOutput(LogLevel.INFO, message, null, "info");
            }
    
            @Override
            public void info(String message, CoreError error) {
                loggerOutput(LogLevel.INFO, message, error, "info");
            }
    
            @Override
            public void info(String message, Throwable thr) {
                loggerOutput(LogLevel.INFO, message, "info", thr);
            }
    
            @Override
            public void debug(String message) {
                loggerOutput(LogLevel.DEBUG, message, null, "debug");
            }

            @Override
            public void debug(String message, CoreError error) {
                loggerOutput(LogLevel.DEBUG, message, error, "debug");
            }
       
            @Override
            public void debug(String message, Throwable thr) {
                loggerOutput(LogLevel.DEBUG, message, "debug", thr);
            }
       
    
            @Override
            public CoreError createError(String message) {
                return createErrorImpl(message);
            }
            
            @Override
            public CoreError createError(Throwable error) {
                return createErrorImpl(error);
            }
        };

    /**
     * Get the Logger object used for publishing log messages and
     * creating error reports for this module.
     *
     * The methods of the Logger object are not thread safe and must
     * be invoked from the module thread.
     * @return The Logger object
     */
    public Logger getLogger() { return logger; }
    
    /**
     * Publish the given log output, including a Throwable to be
     * converted to CoreError and reported.
     * @param level The log level
     * @param message The log message
     * @param methodName The name of the logging method used. This
     *                   information is used to locate the file name
     *                   and line number of the logging call on the
     *                   call stack.
     * @param thr A Throwable to be reported
     */
    private void loggerOutput(LogLevel level, String message,
                              String methodName, Throwable thr) {
        loggerOutput(level, message, createErrorImpl(thr), methodName);
    }
    
    /**
     * Publish the given log output.
     * @param level The log level
     * @param message The log message
     * @param error An optional CoreError error message
     * @param methodName The name of the logging method used. This
     *                   information is used to locate the file name
     *                   and line number of the logging call on the
     *                   call stack.
     */
    private void loggerOutput(LogLevel level, String message,
                              CoreError error, String methodName) {

        // Publish the log message. The header's signal component
        // depends on the log level

        // FIXME: If the application state is not yet RUNNING, the
        // message should be added to a temporary queue which is then
        // flushed when the application state changes to running.
        
        sendMessage(logHeader[level.getNumber()],
                    buildLog(level, message, error, methodName));
        
    }

    /**
     * Build a log message.
     *
     * Package-private scope as this method is also used by the
     * LogCollectorModule class.
     *
     * @param level The log level
     * @param message The log message
     * @param error An optional CoreError error message
     * @param methodName The name of the logging method used. This
     *                   information is used to locate the file name
     *                   and line number of the logging call on the
     *                   call stack.
     * @return The log protobuf message payload
     */
    Log buildLog(LogLevel level, String message, CoreError error,
                 String methodName) {
        // Get the file and line details
        CallSourceLocation location = getCallSourceLocation(methodName, null);

        // Build the log message payload
        Log.Builder builder = Log.newBuilder()
            .setModuleTag(moduleTag)
            .setLogLevel(level)
            .setMessage(message);
        
        if (location.fileName != null && !location.fileName.isEmpty()) {
            builder.setFileName(location.fileName)
                   .setLineNumber(location.lineNumber);
        }
        if (location.className != null && !location.className.isEmpty()) {
            builder.setClassName(location.className);
        }
        if (location.methodName != null && !location.methodName.isEmpty()) {
            builder.setFunctionName(location.methodName);
        }
        if (error != null) {
            builder.setCausedBy(error.buildError());
        }

        // Return the log message

        return builder.build();
        
    }

    /**
     * Implementation of the createError method.
     *
     * Package-private scope as this method is used by the createError
     * methods in both this class and the InterfaceImplementation
     * class.
     */
    CoreError createErrorImpl(String message) {

        // Create the error
        CoreError error = new CoreError(moduleTag, message);

        // And fill in the file and line details
        getCallSourceLocation("createError", null).copyTo(error);
        
        return error;
    }

    /**
     * Implementation of the createError method.
     *
     * Package-private scope as this method is used by the createError
     * methods in both this class and the InterfaceImplementation
     * class.
     */
    CoreError createErrorImpl(Throwable thr) {

        // Create the error
        CoreError error = new CoreError(moduleTag, thr.getMessage());

        // Fill in the stack trace
        StringWriter sw = new StringWriter();
        thr.printStackTrace(new PrintWriter(sw));
        error.details = sw.toString();

        // And fill in the file and line details
        getCallSourceLocation(null, thr.getStackTrace()).copyTo(error);
        
        return error;
    }
    
    /**
     * A struct of the source code location of a log or error
     * occurrence. All elements are optional (Strings can be null or
     * ""). The lineNumber MUST be 0 when fileName is null or "".
     */
    private class CallSourceLocation {
        String className;
        String methodName;
        String fileName;
        int lineNumber;
        void copyTo(CoreError ce) {
            ce.fileName = fileName;
            ce.lineNumber = lineNumber;
            ce.className = className;
            ce.functionName = methodName;
        }
            
    }
    
    /**
     * Traverse the call stack to identify and return the class,
     * method and file name, and line number of the call to the named
     * method.
     * @param methodName The name of the method to locate on the call
     *                   stack. null will just pick the top one.
     * @param stackTrace The stack trace to search. null will examine
     *                   the current thread.
     * @return A CallSourceLocation of class, method, and source file
     *         name as well as and line number
     */
    private CallSourceLocation
        getCallSourceLocation(String methodName,
                              StackTraceElement[] stackTrace) {
        
        CallSourceLocation retVal = new CallSourceLocation();
        
        try {
            if (stackTrace == null) {
                stackTrace = Thread.currentThread().getStackTrace();
            }
            int index = 0;

            if (methodName != null) {
                // Search for the first occurence of the method name on
                // the call stack
                for (; !stackTrace[index].getMethodName().equals(methodName);
                     index++) {}
                
                // Continue popping the call stack to find the first stack
                // frame that does not match the search term
                for (; stackTrace[index].getMethodName().equals(methodName);
                     index++) {}
            }
            
            // This stack frame represents the place, the methodName
            // method was invoked
            StackTraceElement ste = stackTrace[index];

            // If we get this far, we have found the location and can
            // report it back using the CallSourceLocation struct in
            // retVal. If the location could not be found for some
            // obscure reason, an ArrayIndexOutOfBoundsException will
            // be thrown at some point, which will result in the
            // return of an "empty" CallSourceLocation struct.
            
            String fileName = ste.getFileName();
            int lineNumber = ste.getLineNumber();

            retVal.className = ste.getClassName();
            retVal.methodName = ste.getMethodName();
            retVal.fileName = fileName;
            // Filter out the special negative line number values
            retVal.lineNumber = (fileName != null && !fileName.isEmpty()
                                 && lineNumber > 0) ? lineNumber : 0;
        } catch (ArrayIndexOutOfBoundsException e) {}
        
        return retVal;
    }
}



/*

    Stuff from the previous ModuleBase implementation that has not yet
    been re-fitted. Mostly ApplicationState-related management...



  public void startModule() {
    addFunction("Startup", (MetaData metadata) -> {
      switch (metadata.getSignal()) {
        case "PONG":
          if (state == ApplicationState.INITIALIZING) {
            changeState(ApplicationState.READY);
            debug(String.format("%s got PONG.", id));
            sendUnicast(context.getMasterId(), "StartupMaster", "READY", null);
          }
          break;

        case "START":
          if (state == ApplicationState.READY) {
            changeState(ApplicationState.RUNNING);
            debug(String.format("%s is ready.", id));
          }
          break;

        default:
          err(String.format("Unknown signal '%s' sent to the Startup function for module %s.",
              metadata.getSignal(), id));
      }
    });

    addFunction("Shutdown", (MetaData metadata) -> {
      switch (metadata.getSignal()) {
        case "REQUEST":
          changeState(ApplicationState.SHUTDOWN_REQUEST);
          break;

        case "CANCEL":
          if (state == ApplicationState.SHUTDOWN_REQUEST) {
            changeState(ApplicationState.RUNNING);
          }
          break;

        case "FINALIZE":
          if (state == ApplicationState.SHUTDOWN_REQUEST) {
            changeState(ApplicationState.FINALIZING);
          }
          break;

        default:
          err(String.format("Unknown signal '%s' sent to the Shutdown function for module %s.",
              metadata.getSignal(), id));
      }
    });

    super.start();

  }





  private void onShutdownRequest() {
    sendUnicast(context.getMasterId(), "ShutdownMaster", "READY");
  }

  private void sendPing() {
    sendUnicast(context.getMasterId(), "StartupMaster", "PING", null);
  }



  protected void requestShutdown() {
    // Request the master to initiate a shutdown
    sendUnicast(context.getMasterId(), "ShutdownMaster", "REQUEST");
    debug(id + " requests shutdown\n");
  }

  protected void cancelShutdown() {
    // Request that the ongoing shutdown is cancelled
    if (state == ApplicationState.SHUTDOWN_REQUEST) {
      sendUnicast(context.getMasterId(), "ShutdownMaster", "CANCEL");
    }
  }


*/
