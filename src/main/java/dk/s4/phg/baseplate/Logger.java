
package dk.s4.phg.baseplate;


/**
 * PHG Core Logger interface
 *
 * Interface implemented by the ModuleBase class offering PHG Core
 * logging functionality to subsystems of the module.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public interface Logger {

    /**
     * Report a fatal error.
     * 
     * @param message A (preferably) short log message
     */
    void fatal(String message);

    /**
     * Report a fatal error with a CoreError.
     * 
     * @param message A (preferably) short log message
     * @param error A CoreError object with supplemental details
     */
    void fatal(String message, CoreError error);

    /**
     * Report a fatal error with a Throwable.
     * 
     * @param message A (preferably) short log message
     * @param thr A Throwable object with supplemental details
     */
    void fatal(String message, Throwable thr);

    /**
     * Report an error.
     * 
     * @param message A (preferably) short log message
     */
    void error(String message);

    /**
     * Report an error with a CoreError.
     * 
     * @param message A (preferably) short log message
     * @param error A CoreError object with supplemental details
     */
    void error(String message, CoreError error);

    /**
     * Report an error with a Throwable.
     * 
     * @param message A (preferably) short log message
     * @param thr A Throwable object with supplemental details
     */
    void error(String message, Throwable thr);

    /**
     * Report a warning.
     * 
     * @param message A (preferably) short log message
     */
    void warn(String message);

    /**
     * Report a warning with a CoreError.
     * 
     * @param message A (preferably) short log message
     * @param error A CoreError object with supplemental details
     */
    void warn(String message, CoreError error);

    /**
     * Report a warning with a Throwable.
     * 
     * @param message A (preferably) short log message
     * @param thr A Throwable object with supplemental details
     */
    void warn(String message, Throwable thr);

    /**
     * Report an information message.
     * 
     * @param message A (preferably) short log message
     */
    void info(String message);

    /**
     * Report an information message with a CoreError.
     * 
     * @param message A (preferably) short log message
     * @param error A CoreError object with supplemental details
     */
    void info(String message, CoreError error);

    /**
     * Report an information message with a Throwable.
     * 
     * @param message A (preferably) short log message
     * @param thr A Throwable object with supplemental details
     */
    void info(String message, Throwable thr);

    /**
     * Report a debugging message.
     * 
     * @param message A (preferably) short log message
     */
    void debug(String message);

    /**
     * Report a debugging message with a CoreError.
     * 
     * @param message A (preferably) short log message
     * @param error A CoreError object with supplemental details
     */
    void debug(String message, CoreError error);

    /**
     * Report a debugging message with a Throwable.
     * 
     * @param message A (preferably) short log message
     * @param thr A Throwable object with supplemental details
     */
    void debug(String message, Throwable thr);


    /**
     * Creates a CoreError error message representing an error. This
     * method will fill out the following CoreError properties based
     * on the call:
     *
     *  - moduleTag:    The name of the module implementing this Logger.
     *  - message:      Provided as an argument to the method.
     *  - fileName:     The file name of the source file where the
     *                  createError() was located.
     *  - lineNumber:   The line number of the location of createError()
     *                  within the source file.
     *  - className:    The name of the class where the createError()
     *                  was called.
     *  - functionName: The name of the function or method where the
     *                  createError() was called.
     *
     * The caller may proceed to add more information on the error by
     * filling out the details and causedBy properties.
     *
     * Notice that interfaceName and errorCode will _not_ be set by
     * this method. Please use the createError() on the appropriate
     * InterfaceEndpoint object instead, when creating errors that
     * will be communicated on an interface.
     *
     * @param message A short error message
     * @return A CoreError with moduleTag, message, fileName, and
     *        lineNumber filled out.
     */
    CoreError createError(String message);

    /**
     * Creates a CoreError error message representing an error. This
     * method will fill out the following CoreError properties based
     * on the call:
     *
     *  - moduleTag:    The name of the module implementing this Logger.
     *  - message:      The message of the provided Throwable.
     *  - details:      The stack trace of the provided Throwable.
     *  - fileName:     The file name of the source file where the
     *                  Throwable originated.
     *  - lineNumber:   The line number of the location of Throwable
     *                  origin.
     *  - className:    The name of the class where the Throwable
     *                  originated.
     *  - functionName: The name of the function or method where the
     *                  Throwable originated.
     *
     * The caller may proceed to add more information on the error by
     * filling out the details and causedBy properties.
     *
     * Notice that interfaceName and errorCode will _not_ be set by
     * this method. Please use the createError() on the appropriate
     * InterfaceEndpoint object instead, when creating errors that
     * will be communicated on an interface.
     *
     * @param error A Throwable providing message and details
     * @return A CoreError with moduleTag, message, details, fileName,
     *        and lineNumber filled out.
     */
    CoreError createError(Throwable error);

}
