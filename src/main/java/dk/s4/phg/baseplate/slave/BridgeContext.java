package dk.s4.phg.baseplate.slave;

import dk.s4.phg.baseplate.Context;

import org.zeromq.ZMQ;
import org.zeromq.SocketType;

import java.util.Arrays;

/**
 * The PHG Core common Context object for slave baseplates.
 * 
 * The slave baseplate implementation of a Context implements a bridge
 * will forward all communication via a pair of byte streams to a
 * PipingModule attached to the master baseplate.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public abstract class BridgeContext extends Context {

    private ZMQ.Socket in, out, pair_ext, pair_int;
    private boolean killed = false, ext_closed = false;

    /**
     * Create the Context for a slave baseplate.
     * @param baseplateUniqueIndex Must be a small number (no more
     *                             than 8 bits) unique among all
     *                             baseplate instances connected to
     *                             the entire system
     * @throws IllegalArgumentException if the argument is &gt;255
     * @throws NullPointerException if the Context could not be created
     */
    public BridgeContext(int baseplateUniqueIndex) {
        super(baseplateUniqueIndex);
    }

    /**
     * Messages and commands flowing from this bridge towards the
     * reflector via the piping layer and a PipingModule.
     *
     * Simple data format: 1 byte header followed by the payload. The
     * header byte is one of:
     *   0 Ordinary message fragment. This one is "last" in a message
     *   1 Ordinary message fragment. This one will be followed by
     *     more fragments
     *   2 Subscribe message. The payload is the topic subscribing to.
     *   3 Unsubscribe message. The payload is the topic unsubscribing
     *     from.
     *  15 "Kill" command. This must be sent when the bridge is
     *     shutting down and must always be the last output from the
     *     bridge. The command has no payload.
     */
    protected abstract void bridge2reflector(byte[] data);

    /**
     * Messages and commands flowing from this reflector via the
     * PipingModule and the piping layer to this bridge.
     *
     * Simple data format: 1 byte header followed by the payload. The
     * header byte is one of:
     *   0 Ordinary message fragment. This one is "last" in a message
     *   1 Ordinary message fragment. This one will be followed by
     *     more fragments
     *  15 "Kill" command. This must be the last command sent to the
     *     bridge and will cause/allow the bridge to shut down.
     */
    public void reflector2bridge(byte[] data) {
        // When killed, ignore further input.
        if (ext_closed) return;

        // Forward to the bridge thread loop
        pair_ext.send(data, 0);

        // When killed, close the socket
        if (data[0] == 15) {
            ext_closed = true;
            pair_ext.setLinger(1000);
            pair_ext.close();
        }
    }
    
    protected void initialize() {
        // Create and bind the XPUB socket
        out = getZMQContext().createSocket(SocketType.XPUB);
        out.bind(getReflector2module());
    
        // Create, subscribe all, and bind the SUB socket
        in = getZMQContext().createSocket(SocketType.SUB);
        in.subscribe(new byte[0]); // subscribe all messages
        in.bind(getModule2reflector());

        // Create the pair of PAIR sockets
        // - using a pseudo-random unique string as socket URI
        String pairUri = String.format("inproc://pair-%d", hashCode());
        pair_ext = getZMQContext().createSocket(SocketType.PAIR);
        pair_ext.bind(pairUri);
        pair_int = getZMQContext().createSocket(SocketType.PAIR);
        pair_int.connect(pairUri);

        // Bridge implementation:
        
        // Messages from modules are forwarded to the piping layer
        this.registerPollSocket(in, (payload, status) -> {

                // When killed, we stop normal operation and await
                // close()
                if (killed) return;
                
                // Simple data format: 1 byte header followed by
                // the payload. The header is 0 if the message fragment
                // is "last" in a message and 1 if the message
                // fragment will be followed by more fragments.

                byte[] data = new byte[payload.length + 1];
                
                // header
                data[0] = status ? (byte)1 : (byte)0;

                // data
                System.arraycopy(payload, 0, data, 1, payload.length);
                
                // Forward to the piping layer
                bridge2reflector(data);
            });

        // Subscribe/unsubscribe commands from modules are forwarded
        // to the piping layer
        this.registerPollSocket(out, (data, status) -> {

                // When killed, we stop normal operation and await
                // close()
                if (killed) return;
                
                //  Data from the XPUB socket is one byte 0=unsub or
                //  1=sub, followed by topic. Output must be header +
                //  topic where header is 2=sub and 3=unsub, so we
                //  just subtract the first byte from 3.

                data[0] = (byte)(3 - data[0]);
                
                // Forward to the piping layer
                bridge2reflector(data);
            });

        // Messages from the piping layer (arriving at the pair_int
        // socket) are forwarded to modules
        this.registerPollSocket(pair_int, (data, status) -> {

                // When killed, we stop normal operation and await
                // close()
                if (killed) return;
                
                // Simple data format: 1 byte header followed by
                // the payload. The header is:
                //   0 The message fragment is "last" in a message
                //   1 The message fragment will be followed by more
                //     fragments
                //  15 This is a "kill" command

                if (data[0] == 15 && data.length == 1) {
                    // kill command received - send kill command
                    killed = true;
                    bridge2reflector(data);
                    return;
                }
                if (data[0] != 0 && data[0] != 1) {
                    // Malformed input! This is serious - we must
                    // raise a corePanic!
                    throw new IllegalArgumentException
                        ("Malformed input received from piping layer");
                }

                // Forward the input to the out socket
                byte[] payload = Arrays.copyOfRange(data, 1, data.length);
                out.send(payload, data[0] != 0 ? ZMQ.SNDMORE : 0);
            });
        
    }
    
    protected void cleanup() {
        // kill command
        if (!killed) {
            killed = true;
            // send kill command
            bridge2reflector(new byte[] {15});
        }
        
        // The ZeroMQ manual recommends setting a LINGER time of about
        // 1 second to resolve any locks during shutdown
        out.setLinger(1000);
        in.setLinger(1000);
        pair_int.setLinger(1000);
        
        // Close the sockets
        out.close();
        in.close();
        pair_int.close();
    }
}
