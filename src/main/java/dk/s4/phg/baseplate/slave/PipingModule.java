


/**
 * A PHG Module Core module acting as a proxy for one or more modules
 * attached to a slave baseplate (BridgeContext).
 * 
 * The slave baseplate implementation of a Context (BridgeContext)
 * will forward all communication via a pair of byte streams to a
 * PipingModule attached to the master baseplate reflector.
 *
 * *************************************************************
 *
 *   THIS FILE IS JUST A PLACEHOLDER. THIS MODULE HAS NOT BEEN
 *   IMPLEMENTED YET AND IS NOT SCHEDULED FOR IMPLEMENTATION 
 *   ANY TIME SOON!
 *
 *   An implementation of this module can be found in the
 *   native (C++) baseplate.
 *
 * *************************************************************
 *
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
