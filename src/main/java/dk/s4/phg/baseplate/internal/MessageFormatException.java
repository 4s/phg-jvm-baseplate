
package dk.s4.phg.baseplate.internal;

/**
 * The number of message fragments did not match the message type.
 *
 * The header (first fragment) will indicate whether there should be a
 * single fragment (system messages) or two (all other message types).
 * If the actual number of fragments does not match this, a
 * MessageFormatException will be thrown.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class MessageFormatException extends MessagingException {
    public MessageFormatException(String message, Throwable cause) {
        super(message,cause);
    }
}

