
package dk.s4.phg.baseplate.internal;

/**
 * A message was received but no handler was registered for it.
 *
 * This exception is thrown by the dispatcher if no handler
 * (EventImplementation or FunctionImplementation) was registered for
 * an incoming message.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class NoHandlerException extends MessagingException {
    public NoHandlerException(String message, Throwable cause) {
        super(message,cause);
    }
}

