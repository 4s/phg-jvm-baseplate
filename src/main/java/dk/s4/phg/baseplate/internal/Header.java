package dk.s4.phg.baseplate.internal;

import dk.s4.phg.baseplate.Peer;

import java.nio.charset.Charset;

/**
 * The header of a PHG Core message.
 * 
 * This class is used to parse and serialize the first fragment of PHG
 * core messages containing the header of a message.
 *
 * Messages sent over the PHG Core message bus comprises one or two
 * message fragments, where the first fragment is always a header.
 *
 * Messages are sent using one of three addressing modes: Broadcast,
 * Multicast, or Unicast. Messages MUST report the identity of its
 * sender, and Unicast messages MUST report the identity of the
 * receiver. Broadcast and Multicast messages MUST NOT address a
 * receiver. The Broadcast addressing mode is only used for PHG Core
 * system messages from the MasterModule to the ModuleBase class. It
 * is implied by the name of this addressing mode that all modules
 * must receive and handle such messages, hence this is the job of the
 * abstract base class.
 *
 * All headers include a mandatory (non-empty) subject string, which is
 * used to further classify the contents of the payload.
 * 
 * 3 main types of messages are defined:
 *
 *  - System:
 *
 *      System messages are exchanged exclusively between the
 *      ModuleBase class and the MasterModule which is in charge of
 *      maintaining the system/application state. System messages of
 *      the broadcast and unicast subtypes have only a single fragment
 *      and the payload is carried by the "signal" Header field.
 *      Three sub-types exist:
 *
 *      - systemBroadcast: Communication from the master to all other
 *                         modules (using Broadcast addressing mode).
 *      - systemMulticast: Log messages from any module to log
 *                         collector modules. The log message payload
 *                         is in the second fragment.  
 *      - systemUnicast:   Communication between the master and a
 *                         specific module (using Unicast).
 *
 *  - Interface:
 *
 *      Interface messages are defined in the phg-messaging library as
 *      Google Protocol Buffer format. An interface involve two peers,
 *      assuming the "producer" and "consumer" roles respectively. The
 *      subject of an interface message comprises an "interfaceName"
 *      component and a direction suffix. The interfaceName names a
 *      .protobuf file in the phg-messaging library's /interfaces
 *      folder, which contains the definition of the contents of the
 *      second fragment of the message. The direction suffix defines
 *      whether the message is going from producer to consumer or the
 *      reverse. The "signal" Header field is not used in this message
 *      type. Four sub-types exist:
 *
 *      - {p2c|c2p}Unicast:   One-to-one communication reporting
 *                            either an event or a request as defined
 *                            by the payload. A request must also
 *                            define the callbackDeclId which will be
 *                            used by the response (see the Callback
 *                            type below).
 *      - {p2c|c2p}Multicast: Multicast communication reporting
 *                            an event as defined by the payload.
 *
 *  - Callback:
 *
 *      Callbacks are used to handle the "response" part of the
 *      request-response communication pattern defined by the
 *      interfaces in the phg-messaging library's /interfaces folder.
 *      Callbacks are declared in the {p2c|c2p}Unicast messge
 *      containing the request (see above) and invoked using the
 *      callback-type message. A callback may be used multiple times,
 *      and the "signal" Header field is used to mark the last usage
 *      of a callback (when it is never going to be used again).
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class Header {

    /**
     * Low-level addressing mode of PHG Core messages.
     *
     * This enumeration describes what addressing mode was used on the PHG
     * Core message bus for the the transport of a received message.
     */
    public enum AddressingMode {

        /**
         * Message is unicasted (from one Peer to another)
         */
        UNICAST,

        /** 
         * Message is multicasted (to all modules subscribing to the
         * topic)
         */
        MULTICAST,

        /**
         * Message is a broadcast (only used for
         * MasterModule-to-ModuleBase communication)
         */
        BROADCAST
    }

    
    /**
     * The addressing mode of this message when transmitted over the
     * PHG Core message bus
     */
    public final AddressingMode mode;

    /**
     * The identity of the message sender
     */
    public final Peer sender;

    /**
     * The identity of the message receiver
     */
    public final Peer receiver;

    /**
     * The full subject of the message.
     * @see interfaceName and callbackId.
     */
    public final String subject;

    /**
     * The signal is only used by system and callback type messages.
     */
    public final String signal;

    /**
     * If the message contains an interface request, it must declare a
     * callback to be called later, and this element must be a 4-digit
     * upper-case hex value.
     */
    public final Short callbackDeclId;
    
    /**
     * If the message contains an interface request or event, the
     * subject must be [interfaceName].[direction] where direction is
     * either C2P (consumer to producer) or P2C (producer to
     * consumer).
     *
     * When this is the case, interfaceName will contain the first
     * part of the subject. Otherwise, interfaceName will be null.
     */
    public final String interfaceName;

    /**
     * If the message contains an interface response (callback), the
     * subject must be on the form #XXXX, where XXXX is a 16-bit
     * callbackId represented as a 4-digit upper-case hex value.
     *
     * When this is the case, callbackInvokeId will contain this
     * number and isCallback() will return true.
     *
     * @see #isCallback()
     */
    public final short callbackInvokeId;

    /**
     * The serialized version of this Header.
     *
     * This is the byte sequence that is actually transmitted
     * on-the-wire for this Header.
     */
    public final byte[] serialized;

    
    
    /**
     * The separator character used to separate header fields
     */
    public final static String FIELD_SEPARATOR = "_";

    /**
     * The character used to prefix a callback subject
     */
    public final static String CALLBACK = "#";

    /**
     * The string used to suffix a producer-to-consumer interface
     * message.
     */
    public final static String DIRECTION_P2C = ".P2C";

    /**
     * The string used to suffix a consumer-to-producer interface
     * message.
     */
    public final static String DIRECTION_C2P = ".C2P";

    /**
     * The signal used to mark the last use of a callback when the
     * payload is a success response.
     */
    public final static String SIGNAL_LAST_SUCCESS = "OK.LAST";

    /**
     * The signal used to mark that a callback will be used again when
     * the payload is a success response.
     */
    public final static String SIGNAL_MORE_SUCCESS = "OK.MORE";

    /**
     * The signal used to mark the last use of a callback when the
     * payload is an error response.
     */
    public final static String SIGNAL_LAST_ERROR = "ERR.LAST";

    /**
     * The signal used to mark that a callback will be used again when
     * the payload is an error response.
     */
    public final static String SIGNAL_MORE_ERROR = "ERR.MORE";

    
    /**
     * The constructer is held private. All construction must take
     * place using the static methods to ensure correct formatting and
     * content of the header message fragment, according to the overall
     * type of the message.
     */
    private Header(AddressingMode mode, Peer sender, Peer receiver,
                   String subject, String signal, Short callbackDeclId,
                   String interfaceName, short callbackInvokeId,
                   byte[] serialized) {
        this.mode = mode;
        this.sender = sender;
        this.receiver = receiver;
        this.subject = subject;
        this.signal = signal;
        this.callbackDeclId = callbackDeclId;
        this.interfaceName = interfaceName;
        this.callbackInvokeId = callbackInvokeId;
        // serialized is already defined if this Header was constructed
        // by parsing a byte array. Otherwise we need to serialize.
        this.serialized = serialized == null ? toBytes() : serialized;
    }


    /**
     * Build the topic (header prefix) for accepting broadcast
     * messages
     * @return The topic to subscribe
     */
    public static byte[] topicBroadcast() {

        // Addressing mode
        String msg = "B" + FIELD_SEPARATOR;
        
        // Only ASCII characters are allowed in headers, so safe to
        // ignore charset
        return msg.getBytes(Charset.forName("US-ASCII"));
    }

    /**
     * Build the topic (header prefix) for accepting multicast
     * messages for a certain subject
     * @param subject The subject to subscribe to
     * @return The topic to subscribe
     */
    public static byte[] topicMulticast(String subject) {

        // Addressing mode
        String msg = "M" + FIELD_SEPARATOR;

        // Add subject only if provided
        if (subject != null) {
            msg += subject;
            msg += FIELD_SEPARATOR;
        }
        
        // Only ASCII characters are allowed in headers, so safe to
        // ignore charset
        return msg.getBytes(Charset.forName("US-ASCII"));
    }

    /**
     * Build the topic (header prefix) for accepting unicast messages
     * for a certain receiver
     * @param receiver The receiver identity
     * @return The topic to subscribe
     */
    public static byte[] topicUnicast(Peer receiver) {

        // Addressing mode and receiver
        String msg = "U" + toHex(receiver.toInt32(),8) + FIELD_SEPARATOR;

        // Only ASCII characters are allowed in headers, so safe to
        // ignore charset
        return msg.getBytes(Charset.forName("US-ASCII"));
    }


    /**
     * Test if an array is a prefix of this Header.
     * @param topic The array to match with the start of the Header
     * @return true if topic matched a prefix of the Header
     */
    public boolean matchesTopic(byte[] topic) {
        
        // A prefix cannot be longer than the full array
        if (topic.length > serialized.length) return false;

        // Compare byte by byte
        for (int i = 0; i < topic.length; i++) {
            if (topic[i] != serialized[i]) return false;
        }
        return true;
    }

    
    /**
     * Create a callback-type header.
     *
     * Callbacks are used to handle the "response" part of the
     * request-response communication pattern defined by the
     * interfaces in the phg-messaging library's /interfaces folder.
     *
     * @param sender The sender identity
     * @param receiver The receiver identity
     * @param callbackInvokeId The id of the callback
     * @param error true if the callback payload is an Error,
     *              otherwise it is a success
     * @param more The callback will be used again in the future - this
     *             was NOT the last time the callback is used
     * @return The created Header
     */
    public static Header callback(Peer sender, Peer receiver,
                                  short callbackInvokeId, boolean error,
                                  boolean more) {
        return unicast(sender, receiver, CALLBACK +
                       toHex(callbackInvokeId, 4),
                       more? (error? SIGNAL_MORE_ERROR : SIGNAL_MORE_SUCCESS)
                       : (error? SIGNAL_LAST_ERROR : SIGNAL_LAST_SUCCESS),
                       null, null, callbackInvokeId);
    }


    /**
     * Create a c2pUnicast-type header containing an event.
     *
     * Unicasted events do not declare a callback.
     *
     * @param sender The sender identity
     * @param receiver The receiver identity
     * @param interfaceName The name of the interface
     * @return The created Header
     */
    public static Header c2pUnicast(Peer sender, Peer receiver,
                                    String interfaceName) {
        return interfaceUnicast(sender, receiver, interfaceName, null,
                                DIRECTION_C2P);
    }
    
    /**
     * Create a c2pUnicast-type header containing a request.
     *
     * Requests must declare a callback to be used by the associated
     * response.
     *
     * @param sender The sender identity
     * @param receiver The receiver identity
     * @param interfaceName The name of the interface
     * @param callbackDeclId The declared callback id
     * @return The created Header
     */
    public static Header c2pUnicast(Peer sender, Peer receiver,
                                    String interfaceName,
                                    short callbackDeclId) {
        return interfaceUnicast(sender, receiver, interfaceName,
                                callbackDeclId, DIRECTION_C2P);
    }
    
    /**
     * Create a p2cUnicast-type header containing an event.
     *
     * Unicasted events do not declare a callback.
     *
     * @param sender The sender identity
     * @param receiver The receiver identity
     * @param interfaceName The name of the interface
     * @return The created Header
     */
    public static Header p2cUnicast(Peer sender, Peer receiver,
                                    String interfaceName) {
        return interfaceUnicast(sender, receiver, interfaceName, null,
                                DIRECTION_P2C);
    }

    /**
     * Create a p2cUnicast-type header containing a request.
     *
     * Requests must declare a callback to be used by the associated
     * response.
     *
     * @param sender The sender identity
     * @param receiver The receiver identity
     * @param interfaceName The name of the interface
     * @param callbackDeclId The declared callback id
     * @return The created Header
     */
    public static Header p2cUnicast(Peer sender, Peer receiver,
                                    String interfaceName,
                                    short callbackDeclId) {
        return interfaceUnicast(sender, receiver, interfaceName,
                                callbackDeclId, DIRECTION_P2C);
    }

    /**
     * Common part of the {c2p|p2c}Unicast implementation
     */
    private static Header interfaceUnicast(Peer sender, Peer receiver,
                                           String interfaceName,
                                           Short callbackDeclId,
                                           String direction) {
        if (interfaceName == null || interfaceName.isEmpty() ||
            interfaceName.startsWith(CALLBACK)) {
            throw new IllegalArgumentException("Illegal interfaceName");
        }
        return unicast(sender, receiver, interfaceName + direction,
                       null, callbackDeclId, interfaceName, (short)0);
    }
    
    /**
     * Create a c2pMulticast-type header containing an event.
     *
     * @param sender The sender identity
     * @param interfaceName The name of the interface
     * @return The created Header
     */
    public static Header c2pMulticast(Peer sender, String interfaceName) {
        return interfaceMulticast(sender, interfaceName, DIRECTION_C2P);
    }
    
    /**
     * Create a p2cMulticast-type header containing an event.
     *
     * @param sender The sender identity
     * @param interfaceName The name of the interface
     * @return The created Header
     */
    public static Header p2cMulticast(Peer sender, String interfaceName) {
        return interfaceMulticast(sender, interfaceName, DIRECTION_P2C);
    }

    /**
     * Common part of the {c2p|p2c}Multicast implementation
     */
    private static Header interfaceMulticast(Peer sender,
                                             String interfaceName,
                                             String direction) {
        if (interfaceName == null || interfaceName.isEmpty() ||
            interfaceName.startsWith(CALLBACK)) {
            throw new IllegalArgumentException("Illegal interfaceName");
        }
        return multicast(sender, interfaceName + direction, null,
                         interfaceName, (short)0);
    }

    /**
     * A system message from the MasterModule to all other modules
     * (ModuleBase)
     *
     * @param sender The sender identity
     * @param subject The subject of the message (subtype)
     * @param signal The payload of the message
     * @return The created Header
     */
    public static Header systemBroadcast(Peer sender, String subject,
                                         String signal) {
        if (subject == null || subject.isEmpty()) {
            throw new IllegalArgumentException("Subject cannot be empty");
        }
        return broadcast(sender, subject, signal);
    }

    /**
     * A log message from any module to all log collector modules
     *
     * @param sender The sender identity
     * @param subject The subject of the message (subtype)
     * @param signal The payload of the message
     * @return The created Header
     */
    public static Header systemMulticast(Peer sender, String subject,
                                         String signal) {
        if (subject == null || subject.isEmpty()) {
            throw new IllegalArgumentException("Subject cannot be empty");
        }
        return multicast(sender, subject, signal, null, (short)0);
    }

    /**
     * A system message exchanged between the MasterModule and one
     * particular module (ModuleBase).
     *
     * @param sender The sender identity
     * @param receiver The receiver identity
     * @param subject The subject of the message (subtype)
     * @param signal The payload of the message
     * @return The created Header
     */
    public static Header systemUnicast(Peer sender, Peer receiver,
                                       String subject, String signal) {
        if (subject == null || subject.isEmpty()) {
            throw new IllegalArgumentException("Subject cannot be empty");
        }
        return unicast(sender, receiver, subject, signal, null, null,
                       (short)0);
    }

    /**
     * Create a unicast Header
     */
    private static Header unicast(Peer sender, Peer receiver, String subject,
                                  String signal, Short callbackDeclId,
                                  String interfaceName,
                                  short callbackInvokeId) {
        if (sender == null || receiver == null) {
            throw new IllegalArgumentException
                ("Sender and receiver cannot be null");
        }
        return new Header(AddressingMode.UNICAST, sender, receiver,
                          subject, signal, callbackDeclId, interfaceName,
                          callbackInvokeId, null);
    }

    /**
     * Create a multicast Header
     */
    private static Header multicast(Peer sender, String subject, String signal,
                                    String interfaceName, short callbackInvokeId) {
        if (sender == null) {
            throw new IllegalArgumentException("Sender cannot be null");
        }
        return new Header(AddressingMode.MULTICAST, sender, null, subject, signal,
                          null, interfaceName, callbackInvokeId, null);
    }

    /**
     * Create a broadcast Header
     */
    private static Header broadcast(Peer sender, String subject, String signal) {
        if (sender == null) {
            throw new IllegalArgumentException("Sender cannot be null");
        }
        return new Header(AddressingMode.BROADCAST, sender, null,
                          subject, signal, null, null, (short)0, null);
    }

    /**
     * This is a callback type message
     * @return true if this is a callback type message
     */
    public boolean isCallback() {
        if (subject == null) return false;
        return subject.startsWith(CALLBACK);
    }

    /**
     * The callback will be used again in the future (only applicable
     * if isCallback() is true)
     * @return true if this callback will be used again
     */
    public boolean isMore() {
        if (signal == null) return false;
        return signal.equals(SIGNAL_MORE_SUCCESS) ||
            signal.equals(SIGNAL_MORE_ERROR);
    }

    /**
     * The callback payload is an error
     * @return true if this callback payload is an error
     */
    public boolean isError() {
        if (signal == null) return false;
        return signal.equals(SIGNAL_LAST_ERROR) ||
            signal.equals(SIGNAL_MORE_ERROR);
    }

    /**
     * This is a c2p{Unicast|Multicast} type message
     *
     * The <code>mode</code> field may be used to discern between
     * unicast and multicast mesages, if needed.
     * @return true if this is a consumer-to-producer message
     */
    public boolean isC2P() {
        if (subject == null) return false;
        return subject.endsWith(DIRECTION_C2P);
    }

    /**
     * This is a p2c{Unicast|Multicast} type message
     *
     * The <code>mode</code> field may be used to discern between
     * unicast and multicast mesages, if needed.
     * @return true if this is a producer-to-consumer message
     */
    public boolean isP2C() {
        if (subject == null) return false;
        return subject.endsWith(DIRECTION_P2C);
    }

    /**
     * Parse a byte buffer into a valid Header.
     *
     * @param buffer A byte buffer containing a first messaage
     *               fragment.
     * @return The Header representation of the input.
     * @throws HeaderParseException If the byte buffer could not be
     *                              parsed into a valid Header.
     */
    public static Header parse(byte[] buffer) throws HeaderParseException {

        AddressingMode mode = null;
        Peer sender = null, receiver = null;
        String subject = null, signal = null, interfaceName = null;
        Short callbackDeclId = null;
        short callbackInvokeId = 0;

        // Sanity check the length of the buffer before tokenizing
        
        if (buffer.length < 6) {
            throw new HeaderParseException
                ("Buffer too short to contain a valid header", null);
        }

        // Tokenize the buffer into individual strings:
        //  0: receiver, 1: subject, 2: signal, 3: sender, 4: callbackDeclId
        
        String tokens[] = (new String(buffer, 1, buffer.length - 1,
                                      Charset.forName("US-ASCII")))
            .split(FIELD_SEPARATOR, -1);
        if (tokens.length != 5) {
            throw new HeaderParseException
                ("Buffer contains a wrong number of tokens", null);
        }

        // Parse the header addressing mode

        switch (buffer[0]) {
        case 'U':
            mode = AddressingMode.UNICAST;
            break;
        case 'M':
            mode = AddressingMode.MULTICAST;
            break;
        case 'B':
            mode = AddressingMode.BROADCAST;
            break;
        default:
            throw new HeaderParseException
                ("Illegal addressing mode", null);
        }
        
        
        // Parse the receiver token (tokens[0])

        if (mode == AddressingMode.UNICAST) {

            // Receiver mandatory
            if (tokens[0].isEmpty()) {
                throw new HeaderParseException
                    ("Receiver mandatory in unicasted messages", null);
            }
            try {
                receiver = Peer.fromInt32(fromHex(tokens[0],8));
            } catch (NumberFormatException e) {
                throw new HeaderParseException
                    ("Invalid value for receiver", null);
            }
            
        } else {

            // Receiver forbidden
            if (!tokens[0].isEmpty()) {
                throw new HeaderParseException
                    ("Receiver only allowed in unicasted messages", null);
            }
        }
        
        
        // The mandatory subject is in tokens[1]

        if (tokens[1].isEmpty()) {
            throw new HeaderParseException
                ("Subject mandatory in all messages", null);
        }
        subject = tokens[1];
        
        
        // The signal is in tokens[2]

        signal = tokens[2].isEmpty() ? null : tokens[2];
        
        
        // Parse the mandatory sender token (tokens[3])
        
        if (tokens[3].isEmpty()) {
            throw new HeaderParseException
                ("Sender mandatory in all messages", null);
        }
        try {
            sender = Peer.fromInt32(fromHex(tokens[3],8));
        } catch (NumberFormatException e) {
            throw new HeaderParseException
                ("Invalid value for sender", null);
        }
        
        
        // The callbackDeclId is in tokens[4]

        try {
            callbackDeclId = tokens[4].isEmpty() ? null
                                            : (short)fromHex(tokens[4],4);
        } catch (NumberFormatException e) {
            throw new HeaderParseException
                ("Invalid value for callbackDeclId", null);
        }
        
        // Process the header to classify the type of the message

        if (subject.startsWith(CALLBACK)) {

            // Callback type

            // Must be a unicasted messge
            if (mode != AddressingMode.UNICAST) {
                throw new HeaderParseException
                    ("Callbacks must be unicasted", null);
            }

            // CallbackDeclId is forbidden
            if (callbackDeclId != null) {
                throw new HeaderParseException
                    ("Callbacks cannot declare callbacks", null);
            }

            // Signal must be either of SIGNAL_*_*
            if (signal == null || !(signal.equals(SIGNAL_LAST_SUCCESS) ||
                  signal.equals(SIGNAL_MORE_SUCCESS) ||
                  signal.equals(SIGNAL_LAST_ERROR) ||
                  signal.equals(SIGNAL_MORE_ERROR))) {
                throw new HeaderParseException
                    ("Illegal callback signal", null);
            }

            // Extract and parse the callbackInvokeId
            try {
                callbackInvokeId
                    = (short)fromHex(subject.substring(CALLBACK.length()),4);
            } catch (NumberFormatException e) {
                throw new HeaderParseException
                    ("Invalid value for callbackInvokeId", null);
            }
            
        } else if (subject.endsWith(DIRECTION_P2C) ||
                   subject.endsWith(DIRECTION_C2P)) {

            // Interface type
            
            // Must not be a broadcasted message
            if (mode == AddressingMode.BROADCAST) {
                throw new HeaderParseException
                    ("Interface messages cannot be broadcasted", null);
            }
            
            // signal is not used for interface type messages 
            if (signal != null && !signal.isEmpty()) {
                throw new HeaderParseException
                    ("Signal must be empty for interface messages", null);
            }
            
            // callbackDeclId is not allowed for multicasts 
            if (mode != AddressingMode.UNICAST && callbackDeclId != null) {
                throw new HeaderParseException
                    ("Multicasts cannot declare callbacks", null);
            }
            
        } else {

            // System type

            // CallbackDeclId is forbidden
            if (callbackDeclId != null) {
                throw new HeaderParseException
                    ("System messages cannot declare callbacks", null);
            }
        }

        return new Header(mode, sender, receiver, subject, signal,
                          callbackDeclId, interfaceName, callbackInvokeId,
                          buffer);
    }

    /**
     * Create a string representation of the Header identical to the
     * message fragment actually sent over-the-wire.
     *
     * @return The string representation
     */
    @Override
    public String toString() {
        return new String(serialized, Charset.forName("US-ASCII"));
    }
    
    /**
     * Serialize the Header for transmission as the first message
     * fragment of a ZeroMQ message.
     *
     * @return The byte buffer representing the payload of the header.
     */
    private byte[] toBytes() {
        String msg = "";
        if (mode == AddressingMode.UNICAST) {
            msg = "U" + toHex(receiver.toInt32(),8);
        } else if (mode == AddressingMode.MULTICAST) {
            msg = "M";
        } else if (mode == AddressingMode.BROADCAST) {
            msg = "B";
        }

        msg += FIELD_SEPARATOR;

        // subject is mandatory and cannot be null or empty
        msg += subject;
        
        msg += FIELD_SEPARATOR;

        if (signal != null) {
            msg += signal;
        }
        
        msg += FIELD_SEPARATOR;

        if (sender != null) {
            msg += toHex(sender.toInt32(),8);
        }

        msg += FIELD_SEPARATOR;

        if (callbackDeclId != null) {
            msg += toHex(callbackDeclId, 4);
        }

        // Only ASCII characters are allowed in headers, so safe to
        // ignore charset
        return msg.getBytes(Charset.forName("US-ASCII"));
    }


    /**
     * Convert int to a fixed-length upper-case hex string.
     */
    private static String toHex(int i, int stringlen) {
        String s = "00000000" + Integer.toHexString(i).toUpperCase();
        return s.substring(s.length()-stringlen);
    }
    
    /**
     * Convert a fixed-length hex string to int.
     */
    private static int fromHex(String s, int stringlen)
                                    throws NumberFormatException {
        if (s.trim().length() != stringlen) {
            throw new NumberFormatException();
        }
        return Integer.parseUnsignedInt(s,16);
    }

}
