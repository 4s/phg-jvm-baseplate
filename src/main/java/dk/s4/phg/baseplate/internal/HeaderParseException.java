
package dk.s4.phg.baseplate.internal;

/**
 * Attempt to parse a Header from a byte buffer failed.
 *
 * The contents of the byte buffer did not conform to the rules of
 * a valid header.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class HeaderParseException extends MessagingException {
    public HeaderParseException(String message, Throwable cause) {
        super(message,cause);
    }
}
