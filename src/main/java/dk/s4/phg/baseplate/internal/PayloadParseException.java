
package dk.s4.phg.baseplate.internal;

/**
 * Attempt to parse the message payload failed.
 *
 * This can one of the following causes:
 *   - The interface of an ordinary message could not be found
 *   - A problem was reported when parsing the protobuf contents
 *     of an ordinary message
 *   - An invalid subject or signal element in a system message
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class PayloadParseException extends MessagingException {
    public PayloadParseException(String message, Throwable cause) {
        super(message,cause);
    }
}

