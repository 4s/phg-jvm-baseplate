
package dk.s4.phg.baseplate.internal;

/**
 * Something unexpected happened in the messaging system.
 *
 * This is basically a carrier for any unexpected exception in the PHG
 * Core baseplate messaging system.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class UnexpectedMessagingException extends MessagingException {
    public UnexpectedMessagingException(String message, Throwable cause) {
        super(message,cause);
    }
}

