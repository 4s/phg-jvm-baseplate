
package dk.s4.phg.baseplate.internal;

/**
 * Top-level exception capturing all that can go wrong in the PHG Core
 * baseplate messaging system.
 *
 * This class is abstract and the different derived classes offers a
 * more specific cause:
 *
 *  - HeaderParseException: Error when parsing the header
 *  - MessageFormatException: The number of message fragments does not
 *    match the header
 *  - PayloadParseException: Error when parsing the payload
 *  - NoHandlerException: Error when receiving a message and no handler
 *    was registered to handle it.
 *  - UnexpectedMessagingException: Something unexpected happened in
 *    the messaging system.
 *
 * The HeaderParseException, MessageFormatException and
 * PayloadParseException all relate to problems interpreting the input
 * from the piping system. The PayloadParseException may also be
 * caused by flaws in the interface definitions or Google Protocol
 * Buffers. The UnexpectedMessagingException captures everything else,
 * in particular errors that should not be possible. The likely cause
 * of such an exception is probably a programming error in the PHG
 * Core implementation.
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public abstract class MessagingException extends Exception {
    protected MessagingException(String message, Throwable cause) {
        super(message,cause);
    }
}

