package dk.s4.phg.baseplate;

import dk.s4.phg.baseplate.internal.Header;
import dk.s4.phg.messages.classes.log.Log;
import dk.s4.phg.messages.classes.log.LogLevel;

import com.google.protobuf.InvalidProtocolBufferException;

import java.util.Arrays;

/**
 * PHG Core Log collector base class
 *
 * This abstract base class can be extended by the application log
 * collector in order to collect and store or forward all logging
 * events from the PHG Core system.
 *
 * The child class needs to implement the log() method and handle the
 * received LogMessage objects.
 *
 * The log output can be filtered using the setLogCollectingLevel()
 * method.
 *
 * See phg-messages/classes/Log.proto
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public abstract class LogCollectorModule extends ModuleBase {

    /**
     * The log level of a message.
     */
    public enum Level {
        DEBUG(0),
        INFO(1),
        WARN(2),
        ERROR(3),
        FATAL(4);

        /** The level value used in the protobuf message */
        final int levelValue;
        Level(int levelValue) { this.levelValue = levelValue; }
    }

    /**
     * A struct class representing a log message
     */
    public class LogMessage {

        /**
         * The TAG of the module (mandatory)
         */
        public String moduleTag;

        /**
         * The logging level (mandatory)
         */
        public Level level;

        /**
         * The log message (mandatory)
         */
        public String message;

        /**
         * The name of the source file (optional)
         */
        public String fileName;

        /**
         * The line in the fileName (optional - valid if and only if
         * fileName is present)
         */
        public int lineNumber;

        /**
         * The name of the class (optional)
         */
        public String className;

        /**
         * The name of the function/method/procedure (optional)
         */
        public String functionName;

        /**
         * The error causing the message (optional)
         */
        public CoreError causedBy;
    }

    /**
     * The current subscription - used by setLogCollectingLevel()
     */
    private byte[] currentSubscription;
    

    /**
     * Construct the log collector.
     * @param context   The PHG Core common Context object shared by all
     *                  modules connected to the JVM baseplate
     * @param moduleTag The mandatory name of this module used when a
     *                  log or error message is recorded. Also known
     *                  as the TAG. A module named "Foo bar-baz"
     *                  should use the PascalCase equivalent
     *                  "FooBarBaz" as module_tag
     */
    protected LogCollectorModule(Context context, String moduleTag) {
        
        // Create the module
        super(context, moduleTag);

        // Initially, we are subscribing to all log messages
        currentSubscription = Header.topicMulticast("Log");
        subscribe(currentSubscription);
        
    }

    /**
     * Overrides the default handler to intercept log messages
     * @param header The parsed header fragment
     * @param payload The payload fragment (or null if the message
     *                only comprise one fragment).
     * @returm true if the handler did handle the message.
     */
    @Override
    boolean defaultHandler(Header header, byte[] payload) {

        if (payload != null &&
            header.matchesTopic(currentSubscription)) {
            // This is a log message we need to intercept


            try {
                // Handle the message
                handle(Log.parseFrom(payload));
                
                // The message was handled
                return true;
                
            } catch (RuntimeException e) {
                // Log error instead
                handle(buildLog(LogLevel.ERROR, "RuntimeException \"" +
                                e.getMessage() +
                                "\" while parsing and handling log message",
                                null, "buildLog"));
            } catch (InvalidProtocolBufferException e) {
                // Log error instead
                handle(buildLog(LogLevel.ERROR, "Error \"" + e.getMessage()
                                + "\"while parsing log message", null,
                                "buildLog"));
            }
        }
        
        // The message was not handled
        return false;
    }
    
    
    /**
     * Handle a received Log message event
     * @param log The log message protobuffer payload
     */
    private void handle(Log log) {

        // The local struct used to store the message
        LogMessage target = new LogMessage();

        // Copy all the elements from the protobuf object one by one

        // The module TAG
        target.moduleTag = log.getModuleTag();

        // The log level
        switch (log.getLogLevel()) {
        case DEBUG:
            target.level = Level.DEBUG;
            break;
        case INFO:
            target.level = Level.INFO;
            break;
        case WARN:
            target.level = Level.WARN;
            break;
        case ERROR:
            target.level = Level.ERROR;
            break;
        case FATAL:
            target.level = Level.FATAL;
            break;
        }

        // The log message
        target.message = log.getMessage();

        // File name and line number
        if (!log.getFileName().isEmpty()) {
            target.fileName = log.getFileName();
            target.lineNumber = log.getLineNumber();
        }

        // Class name
        if (!log.getClassName().isEmpty()) {
            target.className = log.getClassName();
        }

        // Function name
        if (!log.getFunctionName().isEmpty()) {
            target.functionName = log.getFunctionName();
        }

        // Error object
        if (log.hasCausedBy()) {
            target.causedBy = CoreError.fromError(log.getCausedBy());
        }

        // Forward the log message to the child class log collector
        log(target);
    }
    
    
    /**
     * Process a log message.
     *
     * The child class must implement this abstract method to process
     * a received log message.
     *
     * @param message The log message to process.
     */
    protected abstract void log(LogMessage message);
    

    /**
     * Set the filter level for the log output.
     *
     * Setting a filter level will accept all log messages at the
     * given level and higher. I.e. a level of Level.DEBUG will accept
     * all log messages, whereas a level of Level.ERROR will only
     * accept error and fatal error messages and skip all other
     * messages.
     *
     * @param level The filter level.
     */
    public void setLogCollectingLevel(Level level) {

        // Generate the subscription topic prefix matching the new
        // level
        byte[] topic = Header.topicMulticast("Log");
        // Each level above DEBUG adds a '+' character to the Signal
        // component
        byte[] newSubscription = Arrays.copyOf(topic, topic.length +
                                               level.levelValue);
        Arrays.fill(newSubscription, topic.length, newSubscription.length,
                    (byte)'+');
        
        // Compare to current subscription
        if (!newSubscription.equals(currentSubscription)) {
            // Subscription must be changed
            
            // Unsubscribe old topic
            unsubscribe(currentSubscription);
            
            // Subscribe new topic
            subscribe(newSubscription);
            
            currentSubscription = newSubscription;
        }
    }
}
