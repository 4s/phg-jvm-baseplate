
package dk.s4.phg.baseplate;

import com.google.protobuf.MessageLite;

/**
 * Callback for a module function.
 *
 * A Callback transports the response of a module function from the
 * callee to the caller.
 *
 * When calling a function "Foo", the requester must implement an
 * instance of Callback&lt;FooSuccess&gt; and hand that over to the
 * baseplate (@see InterfaceEndpoint.callFunction()) in order to
 * receive the response back.
 *
 * The Foo implementation must register itself at the
 * InterfaceEndpoint as the handler of Foo requests using
 * FunctionImplementation. It will then receive the
 * Callback&lt;FooSuccess&gt; when the function is invoked, and must
 * use it to send the response back to the caller.
 *
 * The function implementation may choose to use a Callback multiple
 * times. The "more" flag is used by the implementation to indicate
 * that more messages will follow. The "more" flag must be set
 * accordingly prior to invoking either success() or error(). The
 * function implementation MUST ensure that EXACTLY ONE response
 * (either success or error) is sent at the end with the "more" flag
 * set to false. (This is necessary to ensure that all allocated
 * resources are released by the baseplate.)
 *
 * @param <R> The type of the response from the successful function
 *            invocation
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public interface Callback<R extends MessageLite> {
    /**
     * The function invocation returns a successful result
     * 
     * @param payload The return value of the function invocation
     * @param hasMore Indicates if this is the last time the callback is used
     */
    void success(R payload, boolean hasMore);

    /** 
     * The function invocation throws an error.
     *
     * The error is required to define the CoreError.interfaceName and
     * CoreError.errorCode properties according to the implemented
     * interface. Use the InterfaceEndpoint.createError() method on
     * the InterfaceEndpoint where this function resides to create
     * such an error.
     * 
     * @param error The error thrown by the function invocation
     * @param hasMore Indicates if this is the last time the callback is used
     */
    void error(CoreError error, boolean hasMore);
}
