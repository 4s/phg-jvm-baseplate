package dk.s4.phg.baseplate;

import com.google.protobuf.MessageLite;

/**
 * The implementation of a PHG messaging event handler.
 *
 * The handler of a PHG messaging event must implement this interface
 * with the A type parameter set to the protobuf MessageLite type
 * representing the event contents.
 *
 * @param <A> The type of the event argument
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

@FunctionalInterface
public interface EventImplementation<A extends MessageLite> {

    /**
     * An event of the registered type was received.
     *
     * This method accept an incoming event of the registered type.
     *
     * @param args The arguments of the event in the form of a
     *             protobuf MessageLite derived object.
     * @param meta Header information from the received packet.
     */
    void accept(A args, MetaData meta);
}
