
package dk.s4.phg.baseplate;

import dk.s4.phg.baseplate.internal.Header;

/**
 * Meta data describing a received PHG Core message.
 *
 * A message received through the PHG Core message bus comes with a
 * MetaData object containing supplemental header information: the
 * identity of the sender is available from sender(); the addressing
 * mode is available from messageType(); and for callback-type
 * messages, more() will report whether there will be more callback
 * invocations.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


public class MetaData {
    private final boolean unicasted;
    private final Peer sender;
    private final boolean more;
    
    /**
     * Creates a new immutable MetaData object from a message header.
     *
     * @param header The message header
     */
    MetaData(Header header) {
        this.unicasted = header.mode == Header.AddressingMode.UNICAST;
        this.sender = header.sender;
        this.more = header.isMore();
    }

    /**
     * Was this a unicasted or a multicasted message?
     * 
     * @return true if the message was received as a direct
     *              (unicasted) message, and false if it was a
     *              multicasted message.
     */
    public boolean unicasted() { return unicasted; }
    
    /**
     * Get the sender (Peer) of the message.
     * 
     * @return The identity of the source of this message
     */
    public Peer sender() { return sender; }
    
    /**
     * There will be more invocations of this callback.
     *
     * Callbacks may be invoked multiple times. The last time a
     * callback is called, more() will report false. This is obviously
     * only defined for callback messages.
     * 
     * @return True if there will be more invocations of the callback.
     */
    public boolean more() { return more; }
    
}
