package dk.s4.phg.baseplate;

import dk.s4.phg.baseplate.internal.Header;
import dk.s4.phg.baseplate.internal.MessagingException;
import dk.s4.phg.baseplate.internal.NoHandlerException;
import dk.s4.phg.baseplate.internal.PayloadParseException;
import dk.s4.phg.baseplate.internal.MessageFormatException;
import dk.s4.phg.baseplate.internal.UnexpectedMessagingException;

import com.google.protobuf.MessageLite;

import java.util.HashMap;
import java.util.Map;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * PHG Core interface endpoint.
 *
 * PHG Core modules expose one or more interfaces through which they
 * can communicate. Interfaces come in two genders: Producer and
 * consumer. A producer interface can communicate with any and all
 * consumer interfaces of the same type, and vice versa.
 *
 * An InterfaceEndpoint must be created by the module it belongs
 * to. This usually happens in the constructor, and must happen before
 * the module is started (which is usually the last thing the
 * constructor would do).
 *
 * @see ModuleBase#implementsProducerInterface()
 * @see ModuleBase#implementsConsumerInterface()
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class InterfaceEndpoint {

    /**
     * The module that owns this interface.
     */
    private final ModuleBase parent;

    /**
     * The name of this interface, using PascalCase formatting.
     */
    private final String interfaceName;

    /**
     * True if this is the producer implementation of the interface,
     * and false if this is the consumer.
     */
    private final boolean producer;

    /**
     * The fully-qualified package name of the Java package containing
     * the Protoc-generated implementation of the protobuf interface
     * defining this interface.
     */
    private final String packageName;

    /**
     * Input interface main protobuf payload class.
     */
    private final Class<?> inInterface;

    /**
     * Output interface main protobuf payload class.
     */
    private final Class<?> outInterface;

    /**
     * Input interface event protobuf payload class.
     * May be null!
     */
    private final Class<?> inInterfaceEvent;

    /**
     * Output interface event protobuf payload class.
     * May be null!
     */
    private final Class<?> outInterfaceEvent;

    /**
     * Input interface request protobuf payload class.
     * May be null!
     */
    private final Class<?> inInterfaceRequest;

    /**
     * Output interface request protobuf payload class.
     * May be null!
     */
    private final Class<?> outInterfaceRequest;


    /**
     * Construct an InterfaceEndpoint.
     *
     * The constructor has package-private scope, at only the
     * ModuleBase is allowed to create new InterfaceEndpoints.
     *
     * @see ModuleBase#implementsProducerInterface()
     * @see ModuleBase#implementsConsumerInterface()
     *
     * @param parent        The module that owns this interface.
     * @param interfaceName The name of this interface, using
     *                      PascalCase formatting.
     * @param producer      true if this is the producer
     *                      implementation of the interface
     * @throws IllegalStateException if another InterfaceEndpoint was
     *                      already registered for the same interface
     *                      on the same module.
     * @throws IllegalArgumentException if interfaceName is null or
     *                      the interface could not be found.
     */
    InterfaceEndpoint(ModuleBase parent, String interfaceName,
                      boolean producer) {

        // interfaceName cannot be null
        if (interfaceName == null) {
            throw new IllegalArgumentException("interfaceName was null");
        }

        // Save the arguments
        this.parent = parent;
        this.interfaceName = interfaceName;
        this.producer = producer;
        
        // Prepare the name of the package containing the
        // protoc-generated protocol buffer library for this
        // interface.
        String interfaceNameU = camelToUnderscore(interfaceName);
        packageName = "dk.s4.phg.messages.interfaces." + interfaceNameU;

        // Get the orientation of this interface endpoint
        String in = producer ? Header.DIRECTION_C2P : Header.DIRECTION_P2C;
        String out = producer ? Header.DIRECTION_P2C : Header.DIRECTION_C2P;

        // The interface endpoint main protobuf classes must always exist
        try {
            inInterface = Class.forName(packageName + in);
            outInterface = Class.forName(packageName + out);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Interface " + interfaceName +
                                               " could not be found");
        }

        // The event and request protobuf classes are prepared. They
        // may not all exist.
        Class<?> ml = null;
        try {
            ml = Class.forName(packageName + in + "$Event");
        } catch (ClassNotFoundException e) {}
        inInterfaceEvent = ml;

        ml = null;
        try {
            ml = Class.forName(packageName + out + "$Event");
        } catch (ClassNotFoundException e) {}
        outInterfaceEvent = ml;

        ml = null;
        try {
            ml = Class.forName(packageName + in + "$Request");
        } catch (ClassNotFoundException e) {}
        inInterfaceRequest = ml;

        ml = null;
        try {
            ml = Class.forName(packageName + out + "$Request");
        } catch (ClassNotFoundException e) {}
        outInterfaceRequest = ml;

        // Subscribe to the input for this interface from the message
        // bus and register this endpoint as the handler of the
        // messages.
        if (!parent.addReceiverInterface(interfaceName + in, this)) {

            // Another InterfaceEndpoint was already registered for
            // the same interface on the same module. We cannot allow
            // that.
            if (producer) {
                throw new IllegalStateException
                    ("Producer interface for interface " + interfaceName +
                     " has already been registered for this module");
            } else {
                throw new IllegalStateException
                    ("Consumer interface for interface " + interfaceName +
                     " has already been registered for this module");
            }
        }
    }

    
    /**
     * Creates a CoreError error message representing an error. This
     * method will fill out the following CoreError properties based
     * on the call:
     *
     *  - moduleTag:     The name of the module implementing this Logger
     *  - message:       Provided as an argument to the method
     *  - interfaceName: The name of this interface
     *  - errorCode:     Provided as an argument to the method
     *  - fileName:      The file name of the source file where the
     *                   createError() was located
     *  - lineNumber:    The line number of the location of createError()
     *                   within the source file
     *  - className:     The name of the class where the createError()
     *                   was called.
     *  - functionName:  The name of the function or method where the
     *                   createError() was called.
     *
     * The caller may proceed to add more information on the error by
     * filling out the details and causedBy properties.
     *
     * @param errorCode An error code according to the definitions in
     *                  the protobuf file defining this interface
     * @param message   A short error message
     * @return A CoreError with moduleTag, message, interfaceName,
     *         errorCode, fileName, lineNumber, className, and
     *         functionName filled out.
     */
    public CoreError createError(int errorCode, String message) {
        CoreError retval = parent.createErrorImpl(message);
        retval.interfaceName = interfaceName;
        retval.errorCode = errorCode;
        return retval;
    }    

    
    /**
     * The module that owns this interface.
     * @return The parent module
     */
    public ModuleBase getModule() { return parent; }

    
    
    
    /* ************************************************************ */
    /*                                                              */
    /*                      Incoming messages                       */
    /*                                                              */
    /* ************************************************************ */
    

    //
    //          Functions
    // 

    
    /**
     * Struct holding a FunctionImplementation along with its dynamic
     * types
     */
    private class FunctionImplAndType<A extends MessageLite,
                                      R extends MessageLite> {
        FunctionImplementation<? super A,? extends R> handler;
        Class<A> argsType;
        Class<R> retvalType;
    }

    
    /**
     * Map containing all registered function handlers of this
     * interface
     */
    private Map<String,FunctionImplAndType<?,?>> functionHandlers
        = new HashMap<String,FunctionImplAndType<?,?>>(){};


    /**
     * Add a function handler
     * @param <A> The class
     * dk.s4.phg.messages.interfaces.{interfaceName}.{functionName}
     * @param <R> The class
     * dk.s4.phg.messages.interfaces.{interfaceName}.{functionName}Success
     * @param functionName The name of the function using CamelCase
     * notation
     * @param handler The handler of the function, which must accept a
     * payload of type
     * dk.s4.phg.messages.interfaces.{interfaceName}.{functionName}
     * and on success return a payload of type
     * dk.s4.phg.messages.interfaces.{interfaceName}.{functionName}Success
     * @throws IllegalArgumentException If the functionName did not
     * match a known function on this interface, another handler was
     * already registered for the same function, or the generic type
     * arguments of the handler did not match. Notice that due to the
     * type erasure of Java generic types, the handler must directly
     * declare that it implements
     * FunctionImplementation&lt;YourFunction,YourFunctionSuccess&gt;
     * at some point. It is not enough to declare that it implements
     * FunctionImplementation&lt;A,R&gt; where A and R are type
     * variables bound to YourFunction and YourFunctionSuccess by a
     * derived class.
     * @throws UnsupportedOperationException If no incoming events
     * are defined on this interface.
     */
    public <A extends MessageLite, R extends MessageLite>
        void addFunctionHandler(String functionName,
                     FunctionImplementation<? super A,? extends R> handler) {


        // Check that we have input requests
        if (inInterfaceRequest == null) {
            throw new UnsupportedOperationException
                ("This interface has no inbound function requests defined");
        }

        Class<A> requestClass;
        Class<R> responseClass;
        
        // Check that the functionName argument was a known function
        // request and initialize request- and responseClass
        try {
            @SuppressWarnings("unchecked")
                Class<A> argsClass
                = (Class<A>)Class.forName(packageName + "." + functionName);
            requestClass = argsClass;
            @SuppressWarnings("unchecked")
                Class<R> retvalClass
                = (Class<R>)Class.forName(packageName + "." + functionName + "Success");
            responseClass = retvalClass;
            inInterfaceRequest.getMethod("has" + functionName);
        } catch (Exception e ) {
            // ClassNotFoundException, NoSuchMethodException, ClassCastException
            throw new IllegalArgumentException("The function " + functionName +
                     " was not recognized as a member of this interface");
        }

        // Check the type argument of the handler matches the
        // request/response classes
        Type[] handlerParameters =
            getActualTypeArguments(handler.getClass(),
                                   FunctionImplementation.class);
        
        // There must be exactly two type arguments, both must be a
        // class (we do not support variables or wildcards at this
        // point), the first argument must be equal to or a superclass
        // of the requestClass, and the second must be equal to or a
        // subclass of the responseClass
        if (handlerParameters == null || handlerParameters.length != 2 ||
            !(handlerParameters[0] instanceof Class<?>) ||
            !((Class<?>)handlerParameters[0]).isAssignableFrom(requestClass) ||
            !(handlerParameters[1] instanceof Class<?>) ||
            !responseClass.isAssignableFrom((Class<?>)handlerParameters[1])) {
            throw new IllegalArgumentException
                ("The provided handler does not implement " +
                 "FunctionImplementation<" + functionName + "," + functionName
                 + "Success>");
        }


        // Store the handler in the functionHandlers map.
        if (functionHandlers.containsKey(functionName)) {
            // Already registered function handler
            throw new IllegalArgumentException("A function handler for " +
                              functionName + " has already been registered");
        } else {
            FunctionImplAndType<A,R> fiat = new FunctionImplAndType<A,R>();
            fiat.handler = handler;
            fiat.argsType = requestClass;
            fiat.retvalType = responseClass;
            functionHandlers.put(functionName, fiat);
        }
    }

    
    /**
     * Dispatch a message to a registered function handler
     *
     * @param request The protobuf message to dispatch - of type
     *                inInterfaceRequest
     * @param header  The Header object
     * @return true if the message was handled
     * @throws MessagingException If something unexpected happens
     * @throws RuntimeException If the handler throws an exception, it
     *         is passed along
     */
    private boolean requestDispatcher(MessageLite request, Header header)
        throws MessagingException {

        // Walk through the registered function handlers until a match
        // is found. (An alternative implementation could leverage the
        // EventCase enumeration rather than a linear search, however,
        // this would probably only be more efficient for interfaces
        // with a large number of functions - which we don't
        // expect...)
        for (Map.Entry<String,FunctionImplAndType<?,?>> handlerRegistration :
                 functionHandlers.entrySet()) {
            
            // Test if this request type was in the message. The
            // existence of the hasFooBar() method was verified when
            // registering the handler, so this should never fail
            boolean has;
            try {
                has = (Boolean)inInterfaceRequest.getMethod
                    ("has" + handlerRegistration.getKey()).invoke(request);
            } catch (ReflectiveOperationException e) {
                throw new UnexpectedMessagingException
                    ("Unexpected", e);
            }
            if (has) {

                // Found the proper handler, now get the payload
                
                // A Protoc-generated file should always have a
                // getFooBar method if the hasFooBar method exists, so
                // this should also never fail.
                MessageLite args;
                try {
                    args = (MessageLite)inInterfaceRequest.getMethod
                        ("get" + handlerRegistration.getKey())
                        .invoke(request);
                } catch (ReflectiveOperationException e) {
                    throw new PayloadParseException
                        ("Could not extract the message payload", e);
                }

                // Create the callback proxy

                ProxyCallback<?> callback =
                    new ProxyCallback<>(header.sender, header.callbackDeclId,
                              handlerRegistration.getValue().retvalType);

                
                // Create the MetaData object
                MetaData meta = new MetaData(header);

                // Invoke the handler
                
                // The type of the handler was verified when it was
                // registered, so unless there is an error in the
                // interface definition, this should never fail. The
                // following uses reflection to bypass the Java
                // compiler type check. It is the equivalent of:
                //
                //     handlerRegistration.getValue().handler
                //                        .apply(args,callback,meta);

                try {
                    FunctionImplementation.class
                        .getMethod("apply", MessageLite.class, Callback.class,
                                   MetaData.class)
                        .invoke(handlerRegistration.getValue().handler,
                                args, callback, meta);
                } catch (InvocationTargetException e) {
                    if (e.getCause() instanceof RuntimeException) {
                        throw (RuntimeException) e.getCause();
                    } else {
                        throw new UnexpectedMessagingException
                            ("Unexpected", e.getCause());
                    }
                } catch (ReflectiveOperationException e) {
                    throw new UnexpectedMessagingException
                        ("Unexpected", e);
                }

                // We have successfully dispatched the message :-)
                
                return true;
            }
        }

        // No handler matched the received event
        
        return false;
    }


    /**
     * Proxy implementation of the Callback class, representing the
     * remote Callback instance at the caller.
     */
    private class ProxyCallback<R extends MessageLite> implements Callback<R> {
        Peer receiver;
        short callbackId;
        Class<R> payloadClass;
        private boolean isAlive;

        /**
         * Create a ProxyCallback to forward success and error
         * messages to a remote instance.
         * @param receiver     The identity of the module which sent the
         *                     request and is expecting the callback.
         * @param callbackId   The id defined by the receiver
         *                     representing the "real" callback we are
         *                     proxy'ing.
         * @param payloadClass The class of the success response message.
         */
        ProxyCallback(Peer receiver, short callbackId, Class<R> payloadClass) {
            super();
            this.receiver = receiver;
            this.callbackId = callbackId;
            this.payloadClass = payloadClass;
            this.isAlive = true;
        }

        /**
         * The function invocation returns a successful result
         * 
         * @param payload The return value of the function invocation
         * @param hasMore Indicates if this is the last callback-call
         * @throws IllegalStateException If invoked when a response
         * with the more flag set to false has already been sent.
         */
        @Override
        public void success(R payload, boolean hasMore) {
            // Check that the class of payload extends or is equal to payloadClass
            if (!payloadClass.isInstance(payload)) {
                throw new ClassCastException("The payload class did not match this Callback");
            }

            // Send the payload as a success message
            send(false, payload, hasMore);
        }

        /** 
         * The function invocation throws an error.
         *
         * The error is required to define the CoreError.interfaceName
         * and CoreError.errorCode properties according to the
         * implemented interface. Use the
         * InterfaceEndpoint.createError() method on the
         * InterfaceEndpoint where this function resides to create
         * such an error.
         * 
         * @param error The error thrown by the function invocation
         * @param hasMore Indicates if this is the last callback-call
         * @throws IllegalStateException If invoked when a response
         * with the more flag set to false has already been sent.
         */
        @Override
        public void error(CoreError error, boolean hasMore) {
            // Send the error
            send(true, error.buildError(), hasMore);
        }

        /**
         * Send the callback message.
         */
        private void send(boolean error, MessageLite payload, boolean hasMore) {
            // If the callback has already been invoked with hasMore == false it cannot be used again
            if (!isAlive) {
                throw new IllegalStateException
                    ("The callback has already been invoked with more set to false and cannot be used anymore");
            }
            if(!hasMore) {
                // From now on no more calls are allowed
                isAlive = false;
            }
            Header header = Header.callback(parent.getId(), receiver, callbackId, error, hasMore);
            parent.sendMessage(header, payload);
        }
    }

    //
    //          Events
    // 


    /**
     * Map containing all registered event handlers of this interface
     */
    private Map<String,EventImplementation<?>> eventHandlers
        = new HashMap<String,EventImplementation<?>>(){};

    
    /**
     * Add an event handler
     * @param <A> The class
     * dk.s4.phg.messages.interfaces.{interfaceName}.{eventName}
     * @param eventName The name of the event using CamelCase notation
     * @param handler The handler of the event, which must accept a
     * payload of type
     * dk.s4.phg.messages.interfaces.{interfaceName}.{eventName}
     * @throws IllegalArgumentException If the eventName did not match
     * a known event on this interface, another handler was already
     * registered for the same event, or the generic type argument of
     * the handler did not match. Notice that due to the type erasure
     * of Java generic types, the handler must directly declare that
     * it implements EventImplementation&lt;YourEvent&gt; at some
     * point. It is not enough to declare that it implements
     * EventImplementation&lt;T&gt; where T is a type variable bound
     * to YourEvent by a derived class.
     * @throws UnsupportedOperationException If no incoming events
     * are defined on this interface.
     */
    public <A extends MessageLite>
        void addEventHandler(String eventName,
                             EventImplementation<? super A> handler)  {

        // Check that we have input events
        if (inInterfaceEvent == null) {
            throw new UnsupportedOperationException
                ("This interface has no inbound events defined");
        }

        Class<A> eventNameClass;
        
        // Check that the eventName argument was a known event
        try {
            @SuppressWarnings("unchecked")
                Class<A> argsClass
                = (Class<A>)Class.forName(packageName + "." + eventName);
            eventNameClass = argsClass;
            inInterfaceEvent.getMethod("has" + eventName);
        } catch (Exception e ) {
            // ClassNotFoundException, NoSuchMethodException, ClassCastException
            throw new IllegalArgumentException("The event " + eventName +
                     " was not recognized as a member of this interface");
        }

        // Check the type argument of the handler matches the eventNameClass
        Type[] handlerParameters =
            getActualTypeArguments(handler.getClass(),
                                   EventImplementation.class);
        // There must be exactly one type argument, it must be a class
        // (we do not support variables or wildcards at this point),
        // and this must be equal to or a superclass of the
        // eventNameClass
        if (handlerParameters == null || handlerParameters.length != 1 ||
            !(handlerParameters[0] instanceof Class<?>) ||
            !((Class<?>)handlerParameters[0])
            .isAssignableFrom(eventNameClass)) {
            throw new IllegalArgumentException("The provided handler does not "
                         + "implement EventImplementation<" + eventName + ">");
        }
                
        // Store the handler in the eventHandlers map.
        if (eventHandlers.containsKey(eventName)) {
            // Already registered event handler
            throw new IllegalArgumentException("An event handler for " +
                              eventName + " has already been registered");
        } else {
            eventHandlers.put(eventName, handler);
        }
    }


    /**
     * Dispatch a message to a registered event handler
     *
     * @param event  The protobuf message to dispatch - of type
     *               inInterfaceEvent
     * @param header The Header object
     * @return true  if the message was handled
     * @throws MessagingException If something unexpected happens
     * @throws RuntimeException If the handler throws an exception, it
     *         is passed along
     */
    private boolean eventDispatcher(MessageLite event, Header header)
        throws MessagingException {

        // Walk through the registered event handlers until a match is
        // found. (An alternative implementation could leverage the
        // EventCase enumeration rather than a linear search, however,
        // this would probably only be more efficient for interfaces
        // with a large number of events - which we don't expect...)
        for (Map.Entry<String,EventImplementation<?>> handlerRegistration :
                 eventHandlers.entrySet()) {
            
            // Test if this event type was in the message. The
            // existence of the hasFooBar() method was verified when
            // registering the handler, so this should never fail
            boolean has;
            try {
                has = (Boolean)inInterfaceEvent.getMethod
                    ("has" + handlerRegistration.getKey()).invoke(event);
            } catch (ReflectiveOperationException e) {
                throw new UnexpectedMessagingException
                    ("Unexpected", e);
            }
            if (has) {

                // Found the proper handler, now get the payload
                
                // A Protoc-generated file should always have a
                // getFooBar method if the hasFooBar method exists, so
                // this should also never fail.
                MessageLite args;
                try {
                    args = (MessageLite)inInterfaceEvent.getMethod
                        ("get" + handlerRegistration.getKey())
                        .invoke(event);
                } catch (ReflectiveOperationException e) {
                    throw new PayloadParseException
                        ("Could not extract the message payload", e);
                }

                // Create the MetaData object
                MetaData meta = new MetaData(header);
                
                // Invoke the handler
                
                // The type of the handler was verified when it was
                // registered, so unless there is an error in the
                // interface definition, this should never fail. The
                // following uses reflection to bypass the Java
                // compiler type check. It is the equivalent of:
                //
                //     handlerRegistration.getValue().accept(args,meta);

                try {
                    EventImplementation.class
                        .getMethod("accept", MessageLite.class, MetaData.class)
                        .invoke(handlerRegistration.getValue(), args, meta);
                } catch (InvocationTargetException e) {
                    if (e.getCause() instanceof RuntimeException) {
                        throw (RuntimeException) e.getCause();
                    } else {
                        throw new UnexpectedMessagingException
                            ("Unexpected", e.getCause());
                    }
                } catch (ReflectiveOperationException e) {
                    throw new UnexpectedMessagingException
                        ("Unexpected", e);
                }
                
                // We have successfully dispatched the message :-)
                
                return true;
            }
        }
        
        // No handler matched the received event
        
        return false;
    }


    
    
    //
    //          Message dispatcher
    // 


    /**
     * This interface received a message from the communication bus.
     * @param payload The protobuf raw payload data
     * @param header  The Header object
     * @throws MessagingException If something unexpected happens
     * @throws RuntimeException If the handler throws an exception, it
     *         is passed along
     */
    void messageReceived(byte[] payload, Header header)
                                    throws MessagingException {

        // Parse the payload into a Protobuf message
        MessageLite msg;
        try {
            msg = (MessageLite)inInterface.getMethod
                ("parseFrom", byte[].class).invoke(null, payload);
        } catch (InvocationTargetException e) {
            throw new PayloadParseException
                ("Error parsing incoming payload as a " +
                 inInterface.getName() + " message.", e);
        } catch (ReflectiveOperationException e) {
            throw new UnexpectedMessagingException
                ("Unexpected", e);
        }

        boolean match = false;
        try {
            match = (Boolean)inInterface.getMethod("hasEvent")
                .invoke(msg);
        } catch (NoSuchMethodException e) {
            // Ignore; this is a normal situation, and only means
            // that the interface has no events declared
        } catch (ReflectiveOperationException e) {
            // Other exceptions should not be possible
            throw new UnexpectedMessagingException
                ("Unexpected", e);
        }            
        
        if (match) { // Message is an Event

            // Sanity check that events are defined for this interface
            if (inInterfaceEvent == null) {
                throw new PayloadParseException("Error parsing incoming " +
                     inInterface.getName() + " payload: An 'event' element " +
                     "was received, but no 'Event' type is defined.", null);
            }

            // Sanity check that no callbackDeclId is provided
            if (header.callbackDeclId != null) {
                throw new MessageFormatException("Error parsing incoming " +
                     inInterface.getName() + " payload: An 'event' element " +
                     "was received declaring a callback.", null);
            }

            // Extract the Event message
            MessageLite event;
            try {
                event = (MessageLite)inInterface.getMethod("getEvent")
                    .invoke(msg);
            } catch (ReflectiveOperationException e) {
                // Exceptions should not be possible since the "has*"
                // method existed and returned true above
                throw new UnexpectedMessagingException
                    ("Unexpected", e);
            }            

            // Dispatch the event
            if (!eventDispatcher(event, header)) {
                
                // No handler was registered for the event
                throw new NoHandlerException
                    ("No handler was registered to handle an event received " +
                     "on the " + inInterface.getName() + " interface.", null);
            }
            
            
        } else { // Message should be a Request

            try {
                match = (Boolean)inInterface.getMethod("hasRequest")
                    .invoke(msg);
            } catch (NoSuchMethodException e) {
                // Ignore; this is a normal situation, and only means
                // that the interface has no requests declared
            } catch (ReflectiveOperationException e) {
                // Other exceptions should not be possible
                throw new UnexpectedMessagingException
                    ("Unexpected", e);
            }            
        
            if (match) { // Message is a Request

                // Sanity check that requests are defined for this interface
                if (inInterfaceRequest == null) {
                    throw new PayloadParseException
                        ("Error parsing incoming " + inInterface.getName() +
                         " payload: A 'request' element was received, but " +
                         "no 'Request' type is defined.", null);
                }
            
                // Sanity check that a callbackDeclId is provided
                if (header.callbackDeclId == null) {
                    throw new MessageFormatException
                        ("Error parsing incoming " + inInterface.getName() +
                         " payload: A 'request' element was received " +
                         "declaring no callback.", null);
                }

                // Extract the Request message
                MessageLite request;
                try {
                    request = (MessageLite)inInterface.getMethod("getRequest")
                        .invoke(msg);
                } catch (ReflectiveOperationException e) {
                    // Exceptions should not be possible since the "has*"
                    // method existed and returned true above
                    throw new UnexpectedMessagingException
                        ("Unexpected", e);
                }            

                // Dispatch the request
                if (!requestDispatcher(request, header)) {
                
                    // No handler was registered for the request
                    throw new NoHandlerException
                        ("No handler was registered to handle a request " +
                         "received on the " + inInterface.getName() +
                         " interface.", null);
                }

            } else {
                // Message is neither Event nor Request!!
                // This is not allowed...
                throw new PayloadParseException
                    ("The payload of a message on the " + inInterface.getName()
                     + " interface was empty or unrecognized.", null);
            }
        }
    }
    




    /* ************************************************************ */
    /*                                                              */
    /*                      Outgoing messages                       */
    /*                                                              */
    /* ************************************************************ */

    
    //
    //          Functions
    // 


    /**
     * Make a remote function call.
     *
     * Add a function handler
     * @param <A> The class
     * dk.s4.phg.messages.interfaces.{interfaceName}.{functionName}
     * @param <R> The class
     * dk.s4.phg.messages.interfaces.{interfaceName}.{functionName}Success
     * @param functionName The name of the function (PascalCase format)
     * @param args         The protobuf MessageLite object containing
     *                     the function arguments. The arg must be an 
     *                     instance of a class identical to the
     *                     functionName.
     * @param receiver     The unicast receiver of this function.
     * @param callback     The callback receiving one or more
     *                     responses from the function. The callback
     *                     must accept a class named after the
     *                     functionName + "Success"
     * @throws IllegalArgumentException if functionName was not found
     *                     on this interface or the &lt;A&gt; or
     *                     &lt;R&gt; type did not match the
     *                     functionName; or receiver was null.
     * @throws UnsupportedOperationException If no outgoing functions
     *                     are defined on this interface.
     */
    public <A extends MessageLite, R extends MessageLite> void callFunction(
                          String functionName,
                          A args,
                          Peer receiver,
                          Callback<? super R> callback) {

        // Check that we have output functions
        if (outInterfaceRequest == null) {
            throw new UnsupportedOperationException("This interface has no outbound functions defined");
        }

        // Check that the receiver is not null
        if (receiver == null) {
            throw new IllegalArgumentException("The receiver cannot be null");
        }
        
        // Prepare the payload

        // Arguments along with their class
        DynamicTyped<A> arguments = new DynamicTyped<A>();
        arguments.obj = args;

        // Response class
        Class<R> responseClass;
        
        // Construct the Class of the function's arguments
        try {
            // The class is auto-generated by protoc, and we trust
            // that the protoc compiler extended the MessageLite
            // class.
            @SuppressWarnings("unchecked")
                Class<A> argsClass = (Class<A>)Class.forName(packageName + "." + functionName);
            arguments.cls = argsClass;

            @SuppressWarnings("unchecked")
                Class<R> respClass = (Class<R>)Class.forName(packageName + "." + functionName + "Success");
            responseClass = respClass;
        } catch (Exception e ) {
            // ClassNotFoundException, ClassCastException
            
            // The class was not found. The functionName argument must be
            // invalid
            throw new IllegalArgumentException("Unknown functionName: " + functionName);
        }

        
        // Wrap the arguments in an Request message
        DynamicTyped<MessageLite.Builder> requestBuilder;
        try {
            requestBuilder = newBuilder(outInterfaceRequest);
            setField(requestBuilder, functionName, arguments);
        } catch (NoSuchMethodException e) {

            // The method was not found. The functionName argument must be
            // invalid
            throw new IllegalArgumentException("Unknown functionName: " + functionName);

            // Other errors are not expected here and suggest more
            // fundamental problems - perhaps errors in the Protobuf
            // definition of the interface. Use corePanic and bail out.
        } catch (ReflectiveOperationException | ClassCastException e) {
            parent.context.corePanic(parent.getModuleTag(), e, false);
            throw new RuntimeException("Unexpected error", e);
        }


        // Wrap the Request message in the payload (C2P or P2C) message
        DynamicTyped<MessageLite.Builder> payloadBuilder;
        try {
            payloadBuilder = newBuilder(outInterface);
            setField(payloadBuilder, "Request", requestBuilder);
            
            // Errors are not expected here and suggest more
            // fundamental problems - perhaps errors in the Protobuf
            // definition of the interface. Use corePanic and bail
            // out.
        } catch (ReflectiveOperationException | ClassCastException e) {
            parent.context.corePanic(parent.getModuleTag(), e, false);
            throw new RuntimeException("Unexpected error", e);
        }

        // Validate the type of the Callback

        // Check the type argument of the callback matches the responseClass
        Type[] callbackParameters = getActualTypeArguments(callback.getClass(), Callback.class);
        if (callbackParameters == null || callbackParameters.length != 1 ||
            !(callbackParameters[0] instanceof Class<?>) ||
            !((Class<?>)callbackParameters[0])
            .isAssignableFrom(responseClass)) {
            
            throw new IllegalArgumentException("The callback does not implement Callback<" + functionName + "Success>");
        }
        
        // Register the callback
        short callbackDeclId;
        try {
            callbackDeclId = parent.registerCallback(callback, responseClass);
        } catch (UnexpectedMessagingException e) {
            // This error suggests a serious memory leak or another
            // programming error. Use corePanic and bail out.
            parent.context.corePanic(parent.getModuleTag(), e, false);
            throw new RuntimeException("Unexpected error or memory leak", e);
        }

        // Prepare the header
        Header header;
        if (producer) {
            // Direction producer-to-consumer
            header = Header.p2cUnicast(parent.getId(), receiver, interfaceName, callbackDeclId);
        } else {
            // Direction consumer-to-producer
            header = Header.c2pUnicast(parent.getId(), receiver, interfaceName, callbackDeclId);
        }

        // Send the request
        MessageLite payload = payloadBuilder.obj.build();
        parent.sendMessage(header, payload);
    }

    //
    //          Events
    // 

    /**
     * Post a multicast event.
     *
     * @param eventName The name of the event (PascalCase format)
     * @param args      The protobuf MessageLite object containing the
     *                  event arguments. The name of the args's class
     *                  must be identical to the eventName.
     * @throws IllegalArgumentException if eventName was not found on
     *                  this interface or the &lt;A&gt; type did not
     *                  match the eventName.
     */
    public <A extends MessageLite> void postEvent(String eventName, A args) {
        postEvent(eventName,args,null);
    }

    /**
     * Post an event - possibly as unicast.
     *
     * @param eventName The name of the event (PascalCase format)
     * @param args      The protobuf MessageLite object containing the
     *                  event arguments. The name of the args's class
     *                  must be identical to the eventName.
     * @param receiver  The unicast receiver of this event. If null,
     *                  the event is multicasted instead.
     * @throws IllegalArgumentException if eventName was not found on
     *                  this interface or the &lt;A&gt; type did not
     *                  match the eventName.
     * @throws UnsupportedOperationException If no outgoing events
     *                  are defined on this interface.
     */
    public <A extends MessageLite>
        void postEvent(String eventName,
                       A args,
                       Peer receiver) {
        // Check that we have output events
        if (outInterfaceEvent == null) {
            throw new UnsupportedOperationException
                ("This interface has no outbound events defined");
        }

        // Prepare the payload

        // Arguments along with their class
        DynamicTyped<A> arguments = new DynamicTyped<A>();
        arguments.obj = args;

        // Construct the Class of the event's arguments
        try {
            // The class is auto-generated by protoc, and we trust
            // that the protoc compiler extended the MessageLite
            // class.
            @SuppressWarnings("unchecked")
                Class<A> argsClass
                = (Class<A>)Class.forName(packageName + "." + eventName);
            arguments.cls = argsClass;
        } catch (ClassNotFoundException e) {
            
            // The class was not found. The eventName argument must be
            // invalid
            throw new IllegalArgumentException("Unknown eventName: "
                                               + eventName);
        }

        // Wrap the arguments in an Event message
        DynamicTyped<MessageLite.Builder> eventBuilder;
        try {
            eventBuilder = newBuilder(outInterfaceEvent);
            setField(eventBuilder, eventName, arguments);
        } catch (NoSuchMethodException e) {

            // The method was not found. The eventName argument must be
            // invalid
            throw new IllegalArgumentException("Unknown eventName: "
                                               + eventName);

            // Other errors are not expected here and suggest more
            // fundamental problems - perhaps errors in the Protobuf
            // definition of the interface. Use corePanic and bail out.
        } catch (ReflectiveOperationException | ClassCastException e) {
            parent.context.corePanic(parent.getModuleTag(), e, false);
            throw new RuntimeException("Unexpected error", e);
        }

        // Wrap the Event message in the payload (C2P or P2C) message
        DynamicTyped<MessageLite.Builder> payloadBuilder;
        try {
            payloadBuilder = newBuilder(outInterface);
            setField(payloadBuilder, "Event", eventBuilder);
            
            // Errors are not expected here and suggest more
            // fundamental problems - perhaps errors in the Protobuf
            // definition of the interface. Use corePanic and bail
            // out.
        } catch (ReflectiveOperationException | ClassCastException e) {
            parent.context.corePanic(parent.getModuleTag(), e, false);
            throw new RuntimeException("Unexpected error", e);
        }

        // Prepare the header

        Header header;
        if (producer) {
            // Direction producer-to-consumer
            header = receiver == null ?
                // Multicast
                Header.p2cMulticast(parent.getId(), interfaceName) :
                // Unicast
                Header.p2cUnicast(parent.getId(), receiver, interfaceName);
        } else {
            // Direction consumer-to-producer
            header = receiver == null ?
                // Multicast
                Header.c2pMulticast(parent.getId(), interfaceName) :
                // Unicast
                Header.c2pUnicast(parent.getId(), receiver, interfaceName);
        }            

        // Send the event
        
        MessageLite payload = payloadBuilder.obj.build();
        parent.sendMessage(header, payload);
    }


    
    
    /* ************************************************************ */
    /*                                                              */
    /*          Utility and Java reflection helper methods          */
    /*                                                              */
    /* ************************************************************ */

    
    /**
     * Reformat a camelCase or PascalCase source string to
     * underscore_case format.
     * @param s Source string in camelCase or PascalCase format
     * @return The reformatted underscore_case string
     */
    private String camelToUnderscore(String s) {
        return s.substring(0,1).toLowerCase()
            + s.substring(1).replaceAll("([A-Z]*)([A-Z][a-z])", "$1_$2")
            .toLowerCase();
    }
    
    
    /**
     * A dynamically-typed object wrapper.
     *
     * This container is used to bundle an object with its class in
     * order to handle dynamic types with Java reflection
     */
    private class DynamicTyped<T> {
        
        /** The class of obj */
        Class<? extends T> cls;
        
        /** The object wrapped by this container */
        T obj;
    }

    /**
     * Create a Protobuf builder object for a message.
     *
     * All Protobuf Message objects have a newBuilder() method which
     * will return a new builder for that message. This method uses
     * Java reflection to invoke this method on a dynamically-typed
     * object.
     *
     * In theory this method may throw a range of exceptions. If the
     * argument is in fact a MessageLite-derived class, exceptions
     * should not happen unless the classloader has issues locating
     * the class. Note that due to the Java generics "type erasure",
     * the argument could have a different class than what is
     * expected.
     *
     * @param message The Protobuf MessageLite-derived class that we
     *                want to build.
     * @return A DynamicTyped container holding the class and object
     *         of the builder returned from the call.
     */
    private DynamicTyped<MessageLite.Builder>
        newBuilder(Class<?> message)
        throws NoSuchMethodException, IllegalAccessException,
               InvocationTargetException,
               ClassCastException, ClassNotFoundException  {

        // We will return the builder's class along with the builder
        // object
        DynamicTyped<MessageLite.Builder> retval
            = new DynamicTyped<MessageLite.Builder>();

        // The class is auto-generated by protoc, and we trust that
        // the protoc compiler extended the MessageLite.Builder class.
        @SuppressWarnings("unchecked")
            Class<? extends MessageLite.Builder> builderClass
            = (Class<? extends MessageLite.Builder>)
            Class.forName(message.getName() + "$Builder");

        // The builder class
        retval.cls = builderClass;

        // The new builder object - from invoking message.newBuilder()
        retval.obj = (MessageLite.Builder)message.getMethod("newBuilder")
            .invoke(null);
        
        return retval;

    }

    /**
     * Set a field on a dynamically-typed message builder.
     *
     * This method will invoke a setter method on the given
     * dynamically-typed protobuf MessageLite.Builder class to set a
     * field.
     *
     * This method may throw a range of exceptions for instance if the
     * given (field name, value type) pair did not match any setter
     * method on the builder.
     * @param builder   A DynamicTyped container holding the class and
     *                  object of the builder to manipulate
     * @param fieldName A PascalCase name of the field to set. By
     *                  prefixing this name with "set", the resulting
     *                  string must match the name of the setter
     *                  method in the protobuf builder class.
     * @param value     A DynamicTyped container holding the value to
     *                  set. The type of the value must match the type
     *                  of the field, otherwise an exception is
     *                  thrown.
     * @throws NoSuchMethodException If no set* method for the given
     *                  fieldName exist
     * @throws IllegalArgumentException If value.cls and value.obj
     *                  does not match
     * @throws IllegalAccessException Should not happen unless the
     *                  protoc generated code is flawed.
     * @throws InvocationTargetException The setter method throws an
     *                  exception, which should never happen.
     */
    private void setField(DynamicTyped<MessageLite.Builder> builder,
                          String fieldName,
                          DynamicTyped<?> value)
        throws NoSuchMethodException, IllegalAccessException,
               InvocationTargetException, IllegalArgumentException {
        builder.cls.getMethod("set" + fieldName, value.cls)
            .invoke(builder.obj, value.obj);
    }


    /**
     * Check if a class implements a parameterized interface and
     * return the actual type arguments if they were provided.
     *
     * The method will search recursively through the searchClass and
     * all its super classes. It WILL NOT search recursively through
     * implemented interfaces and it WILL NOT track the use of type
     * variables. This implies that either the searchClass itself or
     * one of its super classes must directly declare "implements
     * RawInterface&lt;Foo,Bar&gt; for [Foo,Bar] to be returned.
     *
     * @param searchClass  The class to inspect.
     * @param rawInterface The raw interface to search for
     * @return An array of the actual type arguments provided.
     *         An empty array if none were provided.
     *         null if the searchClass did not implement the rawInterface.
     */
    private Type[] getActualTypeArguments(Class<?> searchClass,
                                          Class<?> rawInterface) { 
        
        for (; !searchClass.equals(Object.class);
             searchClass = searchClass.getSuperclass()) {
            Type[] interfaces = searchClass.getGenericInterfaces();
            for (int i = 0; i < interfaces.length; i++) {
                if (interfaces[i] instanceof ParameterizedType) {
                    ParameterizedType t = (ParameterizedType)interfaces[i];
                    if (rawInterface.equals(t.getRawType())) {
                        return t.getActualTypeArguments();
                    }
                } else if (rawInterface.equals(interfaces[i])) {
                    return new Type[0];
                }
            }
        }
        return null;
    }
}
