
package dk.s4.phg.baseplate;

/**
 * PHG Core peer ID
 *
 * The unique ID of a PHG core module attached to the PHG baseplate
 * which may be used for addressing unicast messages.
 *
 * Module IDs can be compared using .equals() and .hashCode()
 *
 * In Headers, the peer address is represented by the 8-character
 * upper-case-hex-string representation, and in Google Protobuf
 * messages the Peer is transported as a 32-bit integer.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class Peer {

    /**
     * Create a Peer ID based on a 32-bit integer
     *
     * All Peer ID exchanges are using 32-bit integers (int32) as
     * underlying Google Protobuf container.
     *
     * @param intRepresentation The transport form of the
     *                          Peer ID.
     * @return A new Peer ID object matching the given String
     */
    public static Peer fromInt32(int intRepresentation) {
        return new Peer(intRepresentation);
    }

    /**
     * The "hidden" integer representation of this Peer
     */
    private int intRepresentation;
    
    private Peer(int intRepresentation) {
        this.intRepresentation = intRepresentation;
    }
    
    /**
     * Get the 32-bit integer representation of the Peer
     *
     * All Peer ID exchanges are using 32-bit integers (int32) as
     * underlying Google Protobuf container.
     *
     * @return The transport form of the Peer ID.
     */
    public int toInt32() {
        return intRepresentation;
    }

    @Override
    public String toString() {
        return "Peer[" + intRepresentation + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj instanceof Peer) {
            return ((Peer)obj).intRepresentation
                == intRepresentation;
        } else {
            return false;
        }        
    }

    @Override
    public int hashCode() {
        return intRepresentation;
    }
}
