
package dk.s4.phg.baseplate;

import dk.s4.phg.messages.classes.error.Error;

import java.util.HashSet;

/**
 * PHG Core Error container
 *
 * Contains an error that can be (or has been) communicated between
 * modules on the PHG Core message bus. CoreErrors can be nested in
 * order to trace the root cause of a problem across multiple modules.
 *
 * By convention, a CoreError SHOULD NOT be logged at log-level higher
 * than "debug" if it is also forwarded (thrown) to another module. It
 * is the responsibility of the module that chooses to handle the
 * error to log the error in case it is relevant to log the error at
 * this point - which may not be the case. This convention is to avoid
 * having multiple modules logging the same error repeatedly in
 * production code.
 *
 * Constructing a CoreError object is typically done by invoking the
 * createError() method on either an InterfaceEndpoint object (for
 * interface-specified errors) or the ModuleBase itself (Logging
 * interface) for errors that are independent on an interface. This
 * will ensure that all properties are correctly filled, and the
 * caller only has to add optional information (details and causedBy
 * properties).
 *
 * See phg-messages/classes/Error.proto
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

public class CoreError {

    /**
     * The name of the interface. 
     *
     * If a protobuf package name is “s4.messages.interfaces.foo_bar”,
     * this interface_name must be “foo_bar”
     *
     * This property is mandatory for interface-specified errors and
     * not used otherwise.
     *
     * Constructing the CoreError using the
     * InterfaceEndpoint.createError() will auto-fill this property to
     * the correct value.
     */
    public String interfaceName = null;

    /**
     * Machine readable error code.
     *
     * Scoped by interfaceName, and defined in the interface docs.
     *
     * This property is mandatory for interface-specified errors and
     * not used otherwise.
     */
    public int errorCode = 0;

    /**
     * The mandatory name of the module in which this error message was
     * recorded. Also known as the TAG. A module named "Foo bar-baz" should
     * use the PascalCase equivalent "FooBarBaz" as module_tag.
     *
     * Constructing the CoreError using the createError() methods on
     * InterfaceEndpoint or from the Logging interface will auto-fill
     * this property to the correct value.
     */
    public final String moduleTag;

    /**
     * A mandatory short error message (at most a few lines)
     */
    public final String message;

    /**
     * An optional longer (multi-line) detailed description of the error
     * (e.g. a stack trace or dump)
     */
    public String details = null;

    /**
     * The optional name (and perhaps full path) of the source file
     * where the error was detected
     *
     * Constructing the CoreError using the createError() methods on
     * InterfaceEndpoint or from the Logging interface will auto-fill
     * this property to the correct value.
     */
    public String fileName = null;

    /**
     * The line number in the source file where the error was detected
     * (only allowed together with fileName).
     *
     * Constructing the CoreError using the createError() methods on
     * InterfaceEndpoint or from the Logging interface will auto-fill
     * this property to the correct value.
     */
    public int lineNumber = 0;

    /**
     * The optional name (perhaps fully qualified) of the class where
     * the error was detected
     *
     * Constructing the CoreError using the createError() methods on
     * InterfaceEndpoint or from the Logging interface will auto-fill
     * this property to the correct value.
     */
    public String className = null;

    /**
     * The optional name of the function/method/subprocedure where the
     * error was detected
     *
     * Constructing the CoreError using the createError() methods on
     * InterfaceEndpoint or from the Logging interface will auto-fill
     * this property to the correct value.
     */
    public String functionName = null;

    /**
     * An optional nested error that was the root cause of this error.
     */
    public CoreError causedBy = null;

    /**
     * Construct a CoreError with the two mandatory properties set.
     * @param moduleTag The mandatory name of the module which was the
     *                  source of the error message. Also known as the
     *                  TAG. A module named "Foo bar-baz" should use
     *                  the PascalCase equivalent "FooBarBaz" as
     *                  module_tag
     * @param message   The error message. Must be a non-empty string
     * @throws IllegalArgumentException If either argument is null or ""
     */ 
    public CoreError(String moduleTag, String message)
                                       throws IllegalArgumentException {
        if (moduleTag == null || moduleTag.isEmpty() ||
            message == null || message.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.moduleTag = moduleTag;
        this.message = message;
    }

    /**
     * Serialize the CoreError into the Protobuf Error message.
     *
     * If loops are found in the chain of causedBy CoreErrors, the
     * chain will be broken and replaced by a simple message stating
     * that.
     * @return The serialized protobuf error message.
     */
    public Error buildError() {
        return buildError(new HashSet<CoreError>());
    }

    private Error buildError(HashSet<CoreError> causeChain) {
        Error.Builder builder = Error.newBuilder()
            .setModuleTag(moduleTag)
            .setMessage(message);
        if (interfaceName != null && !interfaceName.isEmpty()) {
            builder.setInterfaceName(interfaceName).setErrorCode(errorCode);
        }
        if (details != null && !details.isEmpty()) {
            builder.setDetails(details);
        }
        if (fileName != null && !fileName.isEmpty() && lineNumber > 0) {
            builder.setFileName(fileName).setLineNumber(lineNumber);
        }
        if (className != null && !className.isEmpty()) {
            builder.setClassName(className);
        }
        if (functionName != null && !functionName.isEmpty()) {
            builder.setFunctionName(functionName);
        }
        if (causedBy != null) {
            // Detect loops in the chain of causes. If a loop is found
            // break it and insert an error indicating "a loop in the
            // cause chain" in its place.
            causeChain.add(this);
            if (causeChain.contains(causedBy)) {
                // Loop found
                builder.setCausedBy(Error.newBuilder()
                                    .setModuleTag(Context.TAG)
                                    .setMessage("Cause chain loop detected...")
                                    .build());
            } else {
                // No loop found. Recursively add the causedBy CoreError
                builder.setCausedBy(causedBy.buildError(causeChain));
            }
        }
        return builder.build();
    }

    /**
     * Parse a Protobuf Error message into a new CoreError object.
     * @param err The protobuf error to parse.
     * @return The resulting CoreError object.
     * @throws IllegalArgumentException If the given error (or a
     *         nested error in this error) contains an empty moduleTag
     *         or message element.
     */
    public static CoreError fromError(Error err)
                                     throws IllegalArgumentException {
        String moduleTag = err.getModuleTag();
        String message = err.getMessage();
        CoreError ce = new CoreError(moduleTag, message);
        if (!err.getInterfaceName().isEmpty()) {
            ce.interfaceName = err.getInterfaceName();
            ce.errorCode = err.getErrorCode();
        }
        if (!err.getDetails().isEmpty()) ce.details = err.getDetails();
        if (!err.getFileName().isEmpty()) {
            ce.fileName = err.getFileName();
            ce.lineNumber = err.getLineNumber();
        }
        if (!err.getClassName().isEmpty()) ce.className = err.getClassName();
        if (!err.getFunctionName().isEmpty()) ce.functionName = err.getFunctionName();
        if (err.hasCausedBy()) {
            ce.causedBy = CoreError.fromError(err.getCausedBy());
        }
        return ce;
    }

    /**
     * A simple text representation of the error
     */
    @Override
    public String toString() {
        // We need to avoid loops before converting to string and just
        // re-use the loop detection in buildError() above
        return fromError(buildError()).toString_impl();
    }
    
    /**
     * A simple text representation of the error.
     *
     * The CoreError must be loop-free, otherwise we will recurse
     * forever...
     */
    private String toString_impl() {
        
        StringBuilder sb = new StringBuilder();

        // moduleTag
        sb.append("In " + moduleTag);
        
        // File name and line number
        if (fileName != null && !fileName.isEmpty()) {
            sb.append(" (" + fileName + ":" + lineNumber + ")");
        }

        // Class name
        if (className != null && !className.isEmpty()) {
            sb.append(" " + className);
        }
        
        // Function name
        if (functionName != null && !functionName.isEmpty()) {
            if (className != null && !className.isEmpty()) {
                sb.append("::");
            } else {
                sb.append(" ");
            }
            sb.append(functionName);
        }

        // Interface name and error code
        if (interfaceName != null && !interfaceName.isEmpty()) {
            sb.append(" [" + interfaceName + ":" + errorCode + "]");
        }
        
        // Message
        sb.append("\n" + message);

        // Details
        if (details != null && !details.isEmpty()) {
            sb.append("\n" + details);
        }

        // CausedBy
        if (causedBy != null) {
            sb.append("\nCaused by:\n");
            sb.append(causedBy.toString());
        }
        
        return sb.toString();
    }
}
