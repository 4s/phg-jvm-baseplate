package dk.s4.phg.baseplate;

import com.google.protobuf.MessageLite;
import dk.s4.phg.baseplate.slave.BridgeContext;
import dk.s4.phg.messages.interfaces.time.SolicitClock;
import dk.s4.phg.messages.interfaces.time.StartTimer;
import dk.s4.phg.messages.interfaces.time.StartTimerSuccess;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

public class BridgeContextTest {
    private callableModuleBase callableModule;
    private callbackModuleBase callbackModuleOne;
    private callbackModuleBase callbackModuleTwo;

    @Before
    public void setup() {
        BridgeContextMock context = new BridgeContextMock();
        callableModule = new callableModuleBase(context, "Test");
        callbackModuleOne = new callbackModuleBase(context, "Test2");
        callbackModuleTwo = new callbackModuleBase(context, "Test3");
    }

    @Test
    public void postEvent() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        AtomicBoolean beenHere = new AtomicBoolean(false);
        callbackModuleOne.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received");
            beenHere.set(true);
            latch.countDown();
        });
        callableModule.postEventSingle("SolicitClock", SolicitClock.newBuilder().build(), callbackModuleOne.getId());
        latch.await(5000, TimeUnit.MILLISECONDS);
        assertThat(beenHere.get()).isTrue();
    }


    @Test
    public void postMultiCastEvent() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);
        AtomicBoolean beenHere = new AtomicBoolean(false);
        AtomicBoolean beenHereToo = new AtomicBoolean(false);

        callbackModuleOne.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 1");
            beenHere.set(true);
            latch.countDown();
        });
        callbackModuleTwo.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 2");
            beenHereToo.set(true);
            latch.countDown();
        });
        callableModule.postEventMultiCast("SolicitClock", SolicitClock.newBuilder().build());
        latch.await(5000, TimeUnit.MILLISECONDS);
        assertThat(beenHere.get()).isTrue();
        assertThat(beenHereToo.get()).isTrue();
    }

    @Test()
    public void postSingleEvent() throws InterruptedException {
        CountDownLatch latchOne = new CountDownLatch(1);
        CountDownLatch latchTwo = new CountDownLatch(1);
        AtomicBoolean beenHere = new AtomicBoolean(false);
        AtomicBoolean beenHereToo = new AtomicBoolean(false);
        callbackModuleOne.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 1");
            beenHere.set(true);
            latchOne.countDown();
        });
        callbackModuleTwo.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 2");
            beenHereToo.set(true);
            latchTwo.countDown();
        });
        callableModule.postEventSingle("SolicitClock", SolicitClock.newBuilder().build(), callbackModuleOne.getId());
        latchOne.await(300, TimeUnit.MILLISECONDS);
        latchTwo.await(300, TimeUnit.MILLISECONDS);
        assertThat(beenHere.get()).isTrue();
        assertThat(beenHereToo.get()).isFalse();
    }




    static class BridgeContextMock extends BridgeContext {

        protected BridgeContextMock(){
            super(42+42);
        }

        @Override
        protected void bridge2reflector(byte[] data) {
            if(data[0] != 2 && data[0] != 3)
            reflector2bridge(data);
        }
    }

    static class callableModuleBase extends ModuleBase {
        private final InterfaceEndpoint endpoint;

        protected callableModuleBase(Context context, String moduleTag) {
            super(context, moduleTag);
            endpoint = implementsConsumerInterface("Time");
            start();
        }

        public <A extends MessageLite, R extends MessageLite> void call(String functionName, A args, Peer receiver, Callback<? super R> callback) {
            endpoint.callFunction(functionName, args, receiver, callback);
        }

        public <A extends MessageLite> void postEventSingle(String functionName, A args, Peer receiver) {
            endpoint.postEvent(functionName, args, receiver);
        }

        public <A extends MessageLite> void postEventMultiCast(String functionName, A args) {
            endpoint.postEvent(functionName, args);
        }
    }

    static class callbackModuleBase extends ModuleBase {
        List<Consumer<SolicitClock>> solicitListeners;

        protected callbackModuleBase(Context context, String moduleTag) {
            super(context, moduleTag);
            solicitListeners = new ArrayList<>();
            InterfaceEndpoint endpoint = implementsProducerInterface("Time");

            endpoint.addFunctionHandler("StartTimer", new FunctionImplementation<StartTimer, StartTimerSuccess>() {
                @Override
                public void apply(StartTimer args, Callback<StartTimerSuccess> callback, MetaData meta) {
                    callback.success(StartTimerSuccess.newBuilder().setTimerId(args.getDelayMillis()).build(), false);
                }
            });

            endpoint.addEventHandler("SolicitClock",
                    new EventImplementation<SolicitClock>() {
                        @Override
                        public void accept(SolicitClock args, MetaData meta) {
                            notifySolicitClock(args);
                        }
                    });

            start();
        }

        public void registerSolicitClockListener(Consumer<SolicitClock> listener) {
            solicitListeners.add(listener);
        }

        public void notifySolicitClock(SolicitClock solicitClock) {
            for (Consumer<SolicitClock> solicitListener : solicitListeners) {
                solicitListener.accept(solicitClock);
            }
        }
    }
}
