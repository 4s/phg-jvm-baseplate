package dk.s4.phg.baseplate;

import com.google.protobuf.MessageLite;
import dk.s4.phg.baseplate.master.ReflectorContext;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.Bond;
import dk.s4.phg.messages.interfaces.bluetooth_device_control.BondSuccess;
import dk.s4.phg.messages.interfaces.time.SolicitClock;
import dk.s4.phg.messages.interfaces.time.StartTimer;
import dk.s4.phg.messages.interfaces.time.StartTimerSuccess;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

public class ReflectorContextTest {
    private callableModuleBase callableModule;
    private callbackModuleBase callbackModuleOne;
    private callbackModuleBase callbackModuleTwo;
    private LogCollectorModule logCollectorModule;

    @Before
    public void setup() {
        ReflectorContext context = new ReflectorContext(42);
        callableModule = new callableModuleBase(context, "Test");
        callbackModuleOne = new callbackModuleBase(context, "Test2");
        callbackModuleTwo = new callbackModuleBase(context, "Test3");
        logCollectorModule = new logCollectorModuleMock(context, "Test4");
    }

    @Test
    public void canCreateNewModuleBase() {
        Assertions.assertThat(callableModule).isNotNull();
        assertThat(callableModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);

        Assertions.assertThat(callbackModuleOne).isNotNull();
        assertThat(callbackModuleOne.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);
    }

    @Test
    public void genericInterfacesCanBeExtracted() {
        Type type = new FunctionImplementation<Bond, BondSuccess>() {
            @Override
            public void apply(Bond bond, Callback<BondSuccess> callback, MetaData metaData) {

            }
        }
                .getClass().getGenericInterfaces()[0];
        assertThat(type.getTypeName()).contains("BondSuccess");

        type = new Callback<StartTimerSuccess>() {
            @Override
            public void success(StartTimerSuccess payload, boolean lastCall) {
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
            }
        }
                .getClass().getGenericInterfaces()[0];
        assertThat(type.getTypeName()).contains("StartTimerSuccess");
    }

    @Test
    public void callStartTimerMethod() throws InterruptedException {
        final int message = (int) (Math.random() * 10000);
        CountDownLatch latch = new CountDownLatch(1);
        AtomicInteger id = new AtomicInteger();
        Callback<StartTimerSuccess> callback = new Callback<StartTimerSuccess>() {
            @Override
            public void success(StartTimerSuccess payload, boolean lastCall) {
                id.set(payload.getTimerId());
                System.out.println("Timer Id: " + payload.getTimerId());
                latch.countDown();
            }

            @Override
            public void error(CoreError error, boolean lastCall) {
                System.out.println("Error: " + error.details);
                throw new RuntimeException(error.details);
            }
        };
        callableModule.call("StartTimer", StartTimer.newBuilder().setDelayMillis(message).build(), callbackModuleOne.getId(), callback);
        latch.await(5000, TimeUnit.MILLISECONDS);
        assertThat(id.get()).isEqualTo(message);
    }

    @Test
    public void postEvent() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        AtomicBoolean beenHere = new AtomicBoolean(false);
        callbackModuleOne.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received");
            beenHere.set(true);
            latch.countDown();
        });
        callableModule.postEventSingle("SolicitClock", SolicitClock.newBuilder().build(), callbackModuleOne.getId());
        latch.await(5000, TimeUnit.MILLISECONDS);
        assertThat(beenHere.get()).isTrue();
    }

    @Test
    public void postMultiCastEvent() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);
        AtomicBoolean beenHere = new AtomicBoolean(false);
        AtomicBoolean beenHereToo = new AtomicBoolean(false);

        callbackModuleOne.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 1");
            beenHere.set(true);
            latch.countDown();
        });
        callbackModuleTwo.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 2");
            beenHereToo.set(true);
            latch.countDown();
        });
        callableModule.postEventMultiCast("SolicitClock", SolicitClock.newBuilder().build());
        latch.await(5000, TimeUnit.MILLISECONDS);
        assertThat(beenHere.get()).isTrue();
        assertThat(beenHereToo.get()).isTrue();
    }

    @Test()
    public void postSingleEvent() throws InterruptedException {
        CountDownLatch latchOne = new CountDownLatch(1);
        CountDownLatch latchTwo = new CountDownLatch(1);
        AtomicBoolean beenHere = new AtomicBoolean(false);
        AtomicBoolean beenHereToo = new AtomicBoolean(false);
        callbackModuleOne.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 1");
            beenHere.set(true);
            latchOne.countDown();
        });
        callbackModuleTwo.registerSolicitClockListener(clock -> {
            System.out.println("Solicit clock received by callback module 2");
            beenHereToo.set(true);
            latchTwo.countDown();
        });
        callableModule.postEventSingle("SolicitClock", SolicitClock.newBuilder().build(), callbackModuleOne.getId());
        latchOne.await(200, TimeUnit.MILLISECONDS);
        latchTwo.await(200, TimeUnit.MILLISECONDS);
        assertThat(beenHere.get()).isTrue();
        assertThat(beenHereToo.get()).isFalse();
    }

    static class callableModuleBase extends ModuleBase {
        private final InterfaceEndpoint endpoint;

        protected callableModuleBase(Context context, String moduleTag) {
            super(context, moduleTag);
            endpoint = implementsConsumerInterface("Time");
            start();
        }

        public <A extends MessageLite, R extends MessageLite> void call(String functionName, A args, Peer receiver, Callback<? super R> callback) {
            endpoint.callFunction(functionName, args, receiver, callback);
        }

        public <A extends MessageLite> void postEventSingle(String functionName, A args, Peer receiver) {
            endpoint.postEvent(functionName, args, receiver);
        }

        public <A extends MessageLite> void postEventMultiCast(String functionName, A args) {
            endpoint.postEvent(functionName, args);
        }
    }

    static class callbackModuleBase extends ModuleBase {
        List<Consumer<SolicitClock>> solicitListeners;

        protected callbackModuleBase(Context context, String moduleTag) {
            super(context, moduleTag);
            solicitListeners = new ArrayList<>();
            InterfaceEndpoint endpoint = implementsProducerInterface("Time");

            endpoint.addFunctionHandler("StartTimer", new FunctionImplementation<StartTimer, StartTimerSuccess>() {
                @Override
                public void apply(StartTimer args, Callback<StartTimerSuccess> callback, MetaData meta) {
                    callback.success(StartTimerSuccess.newBuilder().setTimerId(args.getDelayMillis()).build(), false);
                }
            });

            endpoint.addEventHandler("SolicitClock",
                    new EventImplementation<SolicitClock>() {
                        @Override
                        public void accept(SolicitClock args, MetaData meta) {
                            notifySolicitClock(args);
                        }
                    });

            start();
        }

        public void registerSolicitClockListener(Consumer<SolicitClock> listener) {
            solicitListeners.add(listener);
        }

        public void notifySolicitClock(SolicitClock solicitClock) {
            for (Consumer<SolicitClock> solicitListener : solicitListeners) {
                solicitListener.accept(solicitClock);
            }
        }
    }


    static class logCollectorModuleMock extends LogCollectorModule{

        protected logCollectorModuleMock(Context context, String moduleTag) {
            super(context, moduleTag);
        }

        @Override
        protected void log(LogMessage message) {
            System.out.println(message.toString());
        }
    }
}