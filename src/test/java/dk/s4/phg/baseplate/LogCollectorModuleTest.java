package dk.s4.phg.baseplate;

import dk.s4.phg.baseplate.internal.Header;
import dk.s4.phg.baseplate.master.ReflectorContext;
import dk.s4.phg.messages.classes.log.Log;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LogCollectorModuleTest {

    private LogCollectorModule logCollectorModule;
    private static AtomicBoolean wasHere;
    private static LogCollectorModule.Level level;

    @Before
    public void setup() {
        wasHere = new AtomicBoolean();
        ReflectorContext context = new ReflectorContext(42);
        logCollectorModule = new logCollectorModuleMock(context, "Test4");

    }

    @Test
    public void defaultHandler() {
        byte[] message = Log.newBuilder().setMessage("message").build().toByteArray();
        wasHere.set(false);
        Header header = mock(Header.class);
        when(header.matchesTopic(any())).thenReturn(true);
        logCollectorModule.defaultHandler(header,message);
        assertThat(wasHere).isTrue();
    }


    @Test
    public void defaultHandlerWrongHeader() {
        wasHere.set(false);
        assertThat(logCollectorModule.defaultHandler("invalidTopic".getBytes(),"payload".getBytes())).isFalse();
        assertThat(wasHere).isFalse();
    }

    @Test
    public void setLogCollectingLevel() {
        wasHere.set(false);

        byte[] message = Log.newBuilder().setMessage("message").build().toByteArray();


        Header header = mock(Header.class);
        when(header.matchesTopic(any())).then( invocation -> {
            byte[] currentSubscription = (byte[]) invocation.getArguments()[0];
            if(currentSubscription.length > 7){
                return false;
            }
            return true;
        });

        logCollectorModule.setLogCollectingLevel(LogCollectorModule.Level.INFO);

        logCollectorModule.defaultHandler(header,message);
        assertThat(wasHere).isTrue();


        wasHere.set(false);
        logCollectorModule.setLogCollectingLevel(LogCollectorModule.Level.FATAL);
        logCollectorModule.defaultHandler(header,message);
        assertThat(wasHere).isFalse();
    }

    static class logCollectorModuleMock extends LogCollectorModule{

        protected logCollectorModuleMock(Context context, String moduleTag) {
            super(context, moduleTag);
        }

        @Override
        protected void log(LogMessage message) {
            wasHere.set(true);
            level = message.level;
        }
    }
}