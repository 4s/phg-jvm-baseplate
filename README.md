
Baseplate implementation for the Java VM
========================================
This is the baseplate implementation for languages based on the Java
VM.

Documentation
-------------
This is an early prototype and not much documentation is available
at this point apart from inline comments in the source code.

Dependencies
------------
The projects depends on phg-messages and 
JeroMQ (https://github.com/zeromq/jeromq).
These jar files are pulled in by gradle, messages are pulled from
maven.alexandra.dk while JeroMQ is from standard maven repo.

Building
--------
To build a jar file run:
```
gradlew clean build
```

Publishing
----------
To build and publish to maven.alexandra.dk, run:
```
./gradlew clean build publish -PrepoUser=$REPO_USER -PrepoPassword=$PASSWORD
```
With $REPO_USER and $PASSWORD set to valid credentials.

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0

The libraries are released under the following licenses:

* JeroMQ: The Mozilla Public License 2.0 (https://opensource.org/licenses/MPL-2.0)
* Protocol Buffers: The 3-clause BSD license (https://opensource.org/licenses/BSD-3-Clause)